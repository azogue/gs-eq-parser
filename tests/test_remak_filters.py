import json
import logging

import pytest

from gs_eq_parser import GSheetLogic
from .conftest import PATH_LOGIC_FILTERS


@pytest.mark.parametrize(
    ("key_input", "key_output", "use_output_labels"),
    [
        ("input_data_cells_1", "output_data_vars_1", True),
        ("input_data_cells_2", "output_data_vars_2", True),
        ("input_data_vars_1", "output_data_vars_1", True),
        ("input_data_cells_1", "output_data_cells_1", False),
    ],
)
def test_filter_selection(
    sheet_filters_examples: dict,
    key_input: str,
    key_output: str,
    use_output_labels: bool,
):
    """
    Test sheet logic evaluation with different combinations of input/outputs.
    """
    input_data = sheet_filters_examples[key_input]
    logic = GSheetLogic.load(PATH_LOGIC_FILTERS)
    logic.set_input_data(input_data)
    results = logic.solve(use_labels=use_output_labels)
    logging.debug(
        f"RESPONSE: {json.dumps(results, indent=2, ensure_ascii=False)}"
    )
    output_data = sheet_filters_examples[key_output]
    assert results == output_data


def _get_filter_selector_sheet_parameters() -> dict:
    """
    Remak filter selection sheet configuration
    """
    gs_key_remak_filters = "1eAneE5rGI5X-X2PjUC8Q-R0KgdfBu9aDGThMjCtAeWk"
    return {
        "sheet_name": "Výstupy",
        "inputs": {
            "Vstupy!B1:B17": "Vstupy!C1:C17",
            "Vstupy!B23:B28": "Vstupy!C23:C28",
        },
        "outputs": {
            "Výstupy!A30:A65": "Výstupy!B30:B65",
            "Výstupy!A2:A16": "Výstupy!B2:B16",
        },
        "sheet_key": gs_key_remak_filters,
    }


@pytest.mark.private_download
def test_download_remak_filter_selection_sheet(sheet_filters_examples: dict):
    """
    Test download of Remak filter selection sheet
    """
    # Download and parse
    logic = GSheetLogic(**_get_filter_selector_sheet_parameters())

    # Check save
    logic.save(PATH_LOGIC_FILTERS.parent / "new_filters.pkl", use_pickle=True)
    logic.save(PATH_LOGIC_FILTERS)

    # Check re-load
    _ = GSheetLogic.load(
        PATH_LOGIC_FILTERS.parent / "new_filters.pkl", use_pickle=True
    )
    logic = GSheetLogic.load(PATH_LOGIC_FILTERS)
    logic.set_input_data(sheet_filters_examples["input_data_vars_1"])

    # SOLVE SHEET
    results = logic.solve()
    # print(results)

    # Extract report to show input and output data
    _ = logic.report()

    # Check results
    key_output = "output_data_vars_1"
    output_data = sheet_filters_examples[key_output]
    assert results == output_data

    # clean pickle file
    (PATH_LOGIC_FILTERS.parent / "new_filters.pkl").unlink()
