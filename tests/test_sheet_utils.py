import pytest

from gs_eq_parser import GSheetLogic
from .conftest import PATH_LOGIC_FILTERS


@pytest.mark.parametrize(
    "sheet_filepath, function_search, found_results",
    # fmt: off
    [
        (PATH_LOGIC_FILTERS, "CHOOSE", {}),
        (PATH_LOGIC_FILTERS, "HLOOKUP", {}),
        (
            PATH_LOGIC_FILTERS, "IFERROR",
            {"Vystupy!A1:T65": [
                "B4", "B7", "B11", "B13", "B14", "L15", "M15", "N15", "O15",
                "Q15", "L16", "M16", "N16", "O16", "Q16", "L17", "M17", "N17",
                "O17", "Q17", "L18", "M18", "N18", "O18", "Q18", "L20", "M20",
                "N20", "O20", "Q20", "L21", "M21", "N21", "O21", "Q21", "L22",
                "M22", "N22", "O22", "Q22", "L23", "M23", "N23", "O23", "Q23",
                "B30", "B31", "B32"
            ]},
        ),
        (
            PATH_LOGIC_FILTERS, "MATCH",
            {"Vystupy!A1:T65": [
                "L27", "M27", "N27", "O27", "Q27", "L28", "M28", "N28", "O28",
                "Q28", "E32", "F32", "L32", "M32", "N32", "O32", "Q32", "L33",
                "M33", "N33", "O33", "Q33", "L37", "M37", "N37", "O37", "Q37",
                "L38", "M38", "N38", "O38", "Q38", "L42"
            ]},
        ),
    ],
    # fmt: on
)
def test_search_functions_in_sheet(
    sheet_old_examples, sheet_filepath, function_search, found_results
):
    logic = GSheetLogic.load(sheet_filepath)
    found = logic.search_function_usage(function_search)
    assert found_results == found
