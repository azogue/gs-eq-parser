import datetime

import pandas as pd
import pytest

from gs_eq_parser import CellExpressionTree
from gs_eq_parser.cellfunctions import cell_function_now, cell_function_today
from gs_eq_parser.cellref import output_cell_value
from gs_eq_parser.definitions import DATE_FORMAT_OUT
from gs_eq_parser.utils import ensure_numeric_value

# noinspection PyPep8
EXAMPLE_RAW_EQUATIONS = [
    "TODAY()",
    "CONCATENATE(DATA!$C4,MID(E2,1,2),MID(E2,4,2))",
    "VALUE(VLOOKUP($E$2,RR!B:I,4,0))",
    "VALUE(VLOOKUP($E$2,RR!B:I,5,0))",
    "IF(E6<1200,0,90)",
    'IF(AND((E5/E11)>E34*1.2,(E6/E12)>E34*1.2,(E5/E11)>(E41+80),(E6/E12)>(-E39+E55+40)),"OK","velký fan!")',  # noqa
    'IF($E$136>($E$127-$E$114-100),"velká kostka!","OK")',
    "IF( AND($E$9 = 0,$E$51=0,$E$6/$E$12-$E$37+($E$12-1)*44>2*44),1,0)",
    'IF(MID($E$10,1,2)="RH",1,0)',
    "IF(E46<E43*0.5,1,2)",
    'IFERROR(MID($E$10,FIND("+",$E$10,1)+1,6),0)',
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B34,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B35,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B36,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B37,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B38,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B39,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B40,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B41,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B42,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B43,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B44,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B45,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B46,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B47,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B48,FAN!$B$6:$B$28,0)+1,0)",
    "VLOOKUP($E$47,Difusory!$H$3:$T$18,MATCH(Difusory!T3,Difusory!$H$3:$T$3,0),0)",  # noqa
    "VLOOKUP($E$47,Difusory!$H$3:$T$18,MATCH(Difusory!$J$3,Difusory!$H$3:$T$3,0),0)",  # noqa
    'IF(MID($E$10,1,2)="ER",1,0)',
    "VLOOKUP($E$16,'DV-ER'!$B$5:$D$8,MATCH('DV-ER'!C4,'DV-ER'!$B$4:$D$4,0),0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B53,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B54,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B55,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B57,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B58,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B59,FAN!$B$6:$B$28,0)+1,0)",
    "HLOOKUP($E$10,FAN!$B$5:$FC$28,MATCH(B60,FAN!$B$6:$B$28,0)+1,0)",
    'IF( OR($E$2 = "04", $E$2 = "06", $E$2 = "10"),VLOOKUP($E$2,FILTR!$B$25:$E$27,2,0),VLOOKUP(DATA!$B74,filtr_tab,2,0))',  # noqa
    'IF( OR($E$2 = "04", $E$2 = "06", $E$2 = "10"),VLOOKUP($E$2,FILTR!$B$25:$E$27,3,0),VLOOKUP(DATA!$B75,filtr_tab,2,0))',  # noqa
    'IF( OR($E$2 = "04", $E$2 = "06", $E$2 = "10"),VLOOKUP($E$2,FILTR!$B$25:$E$27,4,0),VLOOKUP(DATA!$B76,filtr_tab,2,0))',  # noqa
    'IF(OR($E$2="04",$E$2="06",$E$2="10"),1,VLOOKUP(DATA!$B77,filtr_tab,2,0))',
    'IF( OR($E$2 = "04", $E$2 = "06", $E$2 = "10"),0,VLOOKUP(DATA!$B78,filtr_tab,2,0))',  # noqa
    'IF( OR($E$2 = "04", $E$2 = "06", $E$2 = "10"),0,VLOOKUP(DATA!$B79,filtr_tab,2,0))',  # noqa
    "'VO-CHV'!$C$80",
    "'VO-CHV'!$C$71",
    "'VO-CHV'!$C$76",
    "'VO-CHV'!$C$79",
    "'VO-CHV'!$C$72",
    "'VO-CHV'!$C$75",
    "E83-E86",
    "'VO-CHV'!$C$86",
    "'VO-CHV'!$C$87",
    "'VO-CHV'!$C$73",
    "'VO-CHV'!$C$46",
    "'VO-CHV'!$C$47",
    "'VO-CHV'!$C$52",
    "'VO-CHV'!$C$82",
    "'VO-CHV'!$C$83",
    "'VO-CHV'!$C$92",
    "'VO-CHV'!$C$94",
    "'VO-CHV'!$C$95",
    "VLOOKUP($E$22,TAB_CNTF,2,0)",
    "VLOOKUP($E$22,TAB_CNTF,3,0)",
    "'DR NS'!$E$42",
    "IF(E120>950,1,0)",
    "'DR NS'!$E$47",
    "'DR NS'!$E$54",
    "'DR NS'!$E$45",
    "'DR NS'!$E$46",
    "E5",
    "'DR NS'!E48",
    "'DR NS'!$E$32",
    "'DR NS'!$E$37",
    "'DR NS'!$E$38",
    "'DR NS'!$E$39",
    "VLOOKUP(DATA!$E$22,CNTF!$B$3:$H$30,MATCH(CNTF!$G$3,CNTF!$B$3:$H$3,0),0)",
    "VLOOKUP(DATA!$E$22,CNTF!$B$3:$H$30,MATCH(CNTF!$H$3,CNTF!$B$3:$H$3,0),0)",
    "'DR NS'!$E$49",
    "VLOOKUP($E$22,TAB_CNTF,4,0)",
    "'DR NS'!$E$34",
    "'DR NS'!$E$35",
    "VLOOKUP($E$22,TAB_CNTF,5,0)",
    "'DR NS'!$E$36",
    "'DR NS'!$E$50",
    "'DR NS'!$E$52",
    "'DR NS'!E70",
    "'DR NS'!$E$32",
    "'DR NS'!$E$37",
    "'DR NS'!$E$38",
    "'DR NS'!$E$39",
    "'DR NS'!$E$40",
    "VLOOKUP($E$22,TAB_CNTF,4,0)",
    "VLOOKUP($E$22,TAB_CNTF,5,0)",
    "VLOOKUP(DATA!$E$22,CNTF!$B$3:$H$30,MATCH(CNTF!$G$3,CNTF!$B$3:$H$3,0),0)",
    "VLOOKUP(DATA!$E$22,CNTF!$B$3:$H$30,MATCH(CNTF!$H$3,CNTF!$B$3:$H$3,0),0)",
    "'DR NS'!$E$49",
    "'DR NS'!$E$34",
    "'DR NS'!$E$35",
    "'DR NS'!$E$36",
]  # noqa


def test_cell_expressions():
    bag_expressions = {}
    raw_text_subs = {}
    num_subexpr = 0
    dep_vars = []
    for raw_cell_expr in EXAMPLE_RAW_EQUATIONS:
        c = CellExpressionTree.parse(
            raw_cell_expr,
            local_sheet_name="DATA",
            known_subs=bag_expressions,
            raw_text_subs=raw_text_subs,
        )
        # c.render()
        c2 = CellExpressionTree.parse(
            c.text,
            known_subs=bag_expressions,
            raw_text_subs=raw_text_subs,
            local_sheet_name="DATA",
        )
        # c2.render()
        assert c == c2
        assert c.text == c2.text

        num_subexpr += 1 + len(c.descendants)
        dep_vars += c.dependent_variables

    assert len(bag_expressions) == 276
    assert num_subexpr == 605
    assert len(set(map(lambda x: x.variable.ref, dep_vars))) == 58
    assert len(raw_text_subs) == 155


def test_cell_expressions_properties():
    e1 = CellExpressionTree.parse(
        "IF(_PETR_DATA!E46<_PETR_DATA!E43*0.5,1,2)",
        local_sheet_name="_PETR_DATA",
    )
    assert len(e1.dependent_variables) == 2
    assert len(e1.descendants) == 7
    e1.render(return_string=True)

    e2 = CellExpressionTree.parse(
        "this is not a variable :)", local_sheet_name="any"
    )
    assert len(e2.dependent_variables) == 0
    assert len(e2.descendants) == 0

    # cell references are hashable
    bag = {
        e2.variable: "This is a value cell",
        e1.dependent_variables[0]: "This is a variable cell",
    }
    # expressions are also hashable
    bag_e = {e2: "And expressions too?", e1: "yeah :)"}
    bag_e.pop(e1)
    # print(bag_e)

    # what about mixing?
    bag[e2] = "why not?"


@pytest.mark.parametrize(
    "raw_equation, result",
    [
        ("TEXT(MIN(0.5,1,-7,2)+MAX(-3, -2, 7))", "0"),
        ("TEXT(MIN(0.5,1,-7,2)+MAX(-3, -2, 7))&40", '"040"'),
        ("040&TEXT(MIN(0.5,1,-7,2)+MAX(-3, -2, 7))", '"0400"'),
        ("40*3&TEXT(MIN(0.5,1,-7,2)+MAX(-3, -2, 7))", '"1200"'),
    ],
)
def test_basic_numeric_evaluation(raw_equation, result):
    expr = CellExpressionTree.parse(raw_equation, local_sheet_name="local")
    assert expr.evaluate() == result


@pytest.mark.parametrize(
    "raw_equation, known_values, result, text",
    # fmt: off
    [
        (
            "=TEXT(TODAY())", {},
            cell_function_today().strftime(DATE_FORMAT_OUT),
            "TEXT(TODAY())"
        ),
        (
            "=TODAY() + 2", {},
            cell_function_today() + datetime.timedelta(days=2),
            "TODAY()+2",
        ),
        (
            "=TODAY() + TODAY()", {},
            2 * ensure_numeric_value(cell_function_today()),
            "TODAY()+TODAY()",
        ),
        (
            "=TODAY() + NOW()", {},
            ensure_numeric_value(cell_function_today())
            + ensure_numeric_value(cell_function_now()),
            "TODAY()+NOW()",
        ),
        ("=NOW()-3-NOW()", {}, -3.0, "NOW()-3-NOW()"),
        ('=TEXT(131.64; "00")', {}, '"132"', 'TEXT(131.64; "00")'),
        (
            '="Google has " & TEXT(31353; "#,###") & " employees in 2011"',
            {},
            '"Google has 31,353 employees in 2011"',
            '"Google has " & TEXT(31353; "#,###") & " employees in 2011"',
        ),
        ("=H1!B2^4/H1!B4", {"H1!B2": 2, "H1!B4": 4}, 4, "H1!B2^4/H1!B4"),
        (
            "='H(1)'!B2^4/'H:2'!B4",
            {"H_1!B2": 2, "H_2!B4": 4},
            4,
            "H_1!B2^4/H_2!B4",
        ),
        ("=H_1!B2^4/H_2!B4", {"H_1!B2": 2, "H_2!B4": 4}, 4, "H_1!B2^4/H_2!B4"),
        pytest.param(
            "='H(1)'!B2^4/'H:1'!B2",
            {"H_1!B2": 2, "H1!B2": 4},  # TODO handle slugified name collisions
            4,
            "H_1!B2^4/H_1!B2",
            marks=pytest.mark.xfail(raises=AssertionError),
        ),
        (
            "IF(8<=DATA!E43^3,E43*7 / 1.4,0)",
            {"DATA!E43": 1},
            0,
            "IF(8<=DATA!E43^3,DATA!E43*7 / 1.4,0)",
        ),
        (
            "IF(8<=DATA!E43^3,E43^2 / 1.4,0)",
            {"DATA!E43": 7},
            35.0,
            "IF(8<=DATA!E43^3,DATA!E43^2 / 1.4,0)",
        ),
        (
            "IF(8!=DATA!E43^3,E43*7 / 1.4,0)",
            {"DATA!E43": 2},
            0,
            "IF(8!=DATA!E43^3,DATA!E43*7 / 1.4,0)",
        ),
        (
            "IF(8>=DATA!E43^3,E43*7 + 1.4*DATA!$E43,0)",
            {"DATA!E43": 2},
            16.8,
            "IF(8>=DATA!E43^3,DATA!E43*7 + 1.4*DATA!E43,0)",
        ),
        (
            "IF(8<=DATA!E43^3,E43^2 / 1.4,0)",
            {"DATA!E43": 7},
            35.0,
            "IF(8<=DATA!E43^3,DATA!E43^2 / 1.4,0)",
        ),
        (
            "E43^2^E43*3 + 1.4*DATA!$E43",
            {"DATA!E43": 2},
            50.8,
            "DATA!E43^2^DATA!E43*3 + 1.4*DATA!E43",
        ),
        (
            "(E43^2)^(E43*3) + 1.4*DATA!$E43",
            {"DATA!E43": 2},
            4098.8,
            "(DATA!E43^2)^(DATA!E43*3) + 1.4*DATA!E43",
        ),
        (
            'IFERROR(MID($E$10,FIND("+",$E$10,1)+1,6),0)',
            {"DATA!E10": "RH31C-ZID.DG.CR+114866"},
            "114866",
            'IFERROR(MID(DATA!E10,FIND("+",DATA!E10,1)+1,6),0)',
        ),
        (
            "MIN(-E1;-7.5;ABS(E1))",
            {"DATA!E1": 8},
            -8,
            "MIN(-DATA!E1;-7.5;ABS(DATA!E1))",
        ),
        (
            "MIN(-E1;-7.5;ABS(E1))",
            {"DATA!E1": 8},
            -8,
            "MIN(-DATA!E1;-7.5;ABS(DATA!E1))",
        ),
        (
            "-MIN(-E1;-7.5;ABS(E1))",
            {"DATA!E1": 8},
            8,
            "-MIN(-DATA!E1;-7.5;ABS(DATA!E1))",
        ),
        ("E1-2*E1-7.5", {"DATA!E1": 8}, -15.5, "DATA!E1-(2*DATA!E1)-7.5"),
        ("-(E1-2*E1-7.5)", {"DATA!E1": 8}, 15.5, "-(DATA!E1-(2*DATA!E1)-7.5)"),
        (
            "(E43^2)^(E43*3) + 1.4*DATA!$E43",
            {"DATA!E43": 2},
            4098.8,
            "(DATA!E43^2)^(DATA!E43*3)+1.4*DATA!E43",
        ),
        (
            "-(E1^2)*E3",
            {"DATA!E1": -8, "DATA!E3": 2},
            -128,
            "-((DATA!E1^2)*DATA!E3)",
        ),
        ("-E1-2*E1-7.5", {"DATA!E1": -8}, 16.5, "-DATA!E1-(2*DATA!E1)-7.5"),
        ("-E1-7.5", {"DATA!E1": -7.5}, 0, "-DATA!E1-7.5"),
        (
            "-(-E1-2*E1-7.5)",
            {"DATA!E1": -8},
            -16.5,
            "-(-DATA!E1-(2*DATA!E1)-7.5)",
        ),
        ("-(-E1-7.5)", {"DATA!E1": -7.5}, 0, "-(-DATA!E1-7.5)"),
        (
            "=MATCH(FAN_!$B9&MID(FAN_!AA$3,1,2),OK!$B$2:$S$2,0)",
            {
                "FAN!B9": "fan_LxL",
                "FAN!AA3": "RH25C-6ID.BD.CR",
                "OK!B2:S2": pd.Series(
                    [
                        "Cpro / C",
                        "fan_Dsa",
                        "fan_DN",
                        "fan_B2",
                        "fan_spider_A",
                        "fan_E1",
                        "fan_D1",
                        "fan_LxLER",
                        "fan_L2ER",
                        "fan_LxLGR",
                        "fan_L2GR",
                        "fan_LxLRH",
                        "fan_L2RH",
                        "fan_H",
                        "fan_H1",
                        "fan_A",
                        "fan_T7",
                        "fan_slbl_zavit",
                    ],
                    index=range(1, 19),
                    name=1,
                ),
            },
            12,
            "MATCH(FAN!B9&MID(FAN!AA3,1,2),OK!B2:S2,0)",
        ),
        ("=90-DR_NS!$E$11", {"DR_NS!E11": 5}, 85, "90-DR_NS!E11"),
        (
            "=FLOOR(VO_CHV!C59-VO_CHV!C48-VLOOKUP(VO_CHV!C62;VO_CHV!B6:H17;MATCH(VO_CHV!H6;VO_CHV!B6:H6;0);0)-VO_CHV!C74;5)",  # noqa
            {
                "VO_CHV!C48": 3,
                "VO_CHV!C59": 860,
                "VO_CHV!H6": "J",
                "VO_CHV!C62": "BR.G",
                "VO_CHV!C74": 107,
                "VO_CHV!B6:H6": pd.Series(
                    ["", "A", "B", "C", "D", "E", "J"],
                    name=6,
                    index=range(1, 8),
                ),
                "VO_CHV!B6:H17": pd.DataFrame(
                    [
                        ["", "A", "B", "C", "D", "E", "J"],
                        [
                            "22-7",
                            "25",
                            "21.65",
                            "6.25",
                            "18.75",
                            "10.83",
                            "40",
                        ],
                        [
                            "22-8",
                            "25",
                            "21.65",
                            "6.25",
                            "18.75",
                            "10.83",
                            "40",
                        ],
                        [
                            "22-10",
                            "25",
                            "21.65",
                            "6.25",
                            "18.75",
                            "10.83",
                            "40",
                        ],
                        [
                            "30-12",
                            "35",
                            "30.31",
                            "8.75",
                            "26.25",
                            "15.16",
                            "40",
                        ],
                        ["35-16", "40", "34.65", "10", "30", "17.32", "40"],
                        ["BR.W", "38", "32.91", "9.5", "19", "16.46", "40"],
                        ["BR.G", "38", "32.91", "9.5", "19", "16.46", "40"],
                        ["HR.W", "60", "30", "15", "30", "15", "40"],
                        ["HS.W", "30", "30", "15", "0", "15", "40"],
                        ["HZ.G", "30", "30", "15", "0", "15", "40"],
                        ["", "", "", "", "", "", ""],
                    ],
                    columns=range(1, 8),
                    index=range(5, 17),
                ),
            },
            710,
            "FLOOR(VO_CHV!C59-VO_CHV!C48-VLOOKUP(VO_CHV!C62;VO_CHV!B6:H17;MATCH(VO_CHV!H6;VO_CHV!B6:H6;0);0)-VO_CHV!C74;5)",  # noqa
        ),
        ("=F11-2-F11*2", {"DATA!F11": 100}, -102, "DATA!F11-2-DATA!F11*2"),
        (
            "=F11-2-F13*2",
            {"DATA!F11": 1000.1, "DATA!F13": 400},
            198.10000000000002,
            "DATA!F11-2-DATA!F13*2",
        ),
        (
            "=F11-2-F13*INT(F11/F13)",
            {"DATA!F11": 1000.1, "DATA!F13": 400},
            198.10000000000002,
            "DATA!F11-2-DATA!F13*INT(DATA!F11/DATA!F13)",
        ),
        (
            "=IF(F11-2-F13*INT(F11/F13)>(A2-1),A2,0.01)",
            {"DATA!A2": 287, "DATA!F11": 1296.1, "DATA!F13": 592},
            0.01,
            "IF(DATA!F11-2-DATA!F13*INT(DATA!F11/DATA!F13)>(DATA!A2-1),DATA!A2,0.01)",  # noqa
        ),
        (
                "=B11/B12/B9",
                {"DATA!B11": 360, "DATA!B12": 12, "DATA!B9": 12},
                2.5, "(DATA!B11/DATA!B12)/DATA!B9"),
        (
                "=B11&(B12+B9)",
                {"DATA!B11": 360, "DATA!B12": 12, "DATA!B9": 12},
                '"36024"', "DATA!B11&DATA!B12+DATA!B9",
        ),
        (
                "=B11&B12+B9",
                {"DATA!B11": 360, "DATA!B12": 12, "DATA!B9": 12},
                '"36024"', "DATA!B11&DATA!B12+DATA!B9",
        ),
        (
                "=B11&B12+B9&B9",
                {"DATA!B11": 360, "DATA!B12": 12, "DATA!B9": 12},
                '"3602412"', "DATA!B11&DATA!B12+DATA!B9&DATA!B9",
        ),
    ],
    # fmt: on
)
def test_cell_simple_expressions(raw_equation, known_values, result, text):
    c = CellExpressionTree.parse(raw_equation, local_sheet_name="DATA")
    res = c.evaluate(known_values)
    if res != result:
        c.render()
    if isinstance(result, float):
        assert abs(res - result) < 0.0001
    else:
        assert res == result
        assert output_cell_value(res, force_str=True) == output_cell_value(
            result, force_str=True
        )
    # print(c.text)
    c2 = CellExpressionTree.parse(c.text, local_sheet_name="DATA")
    # c2.render()
    assert c == c2
    _ = c.render(return_string=True)


@pytest.mark.parametrize(
    "raw_equation, text",
    # fmt: off
    [
        ('=IF(A1,"","option if A1 is false, you know!")', "=IF(A1,STRING!A1,STRING!A2)"),  # noqa
        ('=IF(A1,"""","option if A1 is false, you know!")', "=IF(A1,STRING!A1,STRING!A2)"),  # noqa
        ('=IF(A1;""""";"option if A1 is false; you know!")', "=IF(A1;STRING!A1;STRING!A2)"),  # noqa
        ('="prefix"&A1&"the rest"', "=STRING!A1&A1&STRING!A2"),
        ('="""&A1&"""', "=STRING!A1&A1&STRING!A1"),
        ('="""&"var_name"&"""', "=STRING!A1&STRING!A2&STRING!A1"),
        ('="""&"var_name"&"crazy_bad"!A3', '=STRING!A1&STRING!A2&"crazy_bad"!A3'),  # noqa
        ('="""&"var_name"&\'crazy_bad\'!A3', "=STRING!A1&STRING!A2&'crazy_bad'!A3"),  # noqa
    ],
    # fmt: on
)
def test_cell_expressions_with_strings(raw_equation, text):
    from gs_eq_parser.cellexpression import (
        _clean_equation_extracting_cell_strings,
    )

    known_values, raw_text_subs = {}, {}
    clean_eq = _clean_equation_extracting_cell_strings(
        raw_equation,
        sheet_name="ANY",
        known_subs=known_values,
        raw_text_subs=raw_text_subs,
    )
    assert clean_eq == text
