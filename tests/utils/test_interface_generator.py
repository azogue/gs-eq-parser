"""Unit tests for code generation utils to build attrs interfaces."""
import importlib.util
from pathlib import Path

import attr
import pytest
from tests.conftest import (
    PATH_LOGIC_EXTERNAL_LINK,
    PATH_LOGIC_FILTERS,
    PATH_LOGIC_OLD_FILTERS_1,
    PATH_LOGIC_OLD_FILTERS_2,
)
from utils.interface_generator import (
    generate_attrs_interface,
    PATH_ADS_TEMPLATE,
)

from gs_eq_parser import GSheetLogic


def _load_dataclasses_from_generated_code(
    path_to_gen_code: Path, request_cls_name: str, response_cls_name: str
):
    """
    Load the gen. code with importlib and extract the 2 new classes from there.
    """
    spec = importlib.util.spec_from_file_location(
        path_to_gen_code.name[:-3], str(path_to_gen_code)
    )
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)

    request_cls = getattr(mod, request_cls_name)
    resp_cls = getattr(mod, response_cls_name)
    return request_cls, resp_cls


def _sheet_workflow_with_dataclass(logic: GSheetLogic, request_cls, resp_cls):
    """
    Demo of the workflow using dataclasses for the sheet logic I/O.

    1. Request object generates input data for sheet logic: `.to_input_cells()`
    2. Set input data for new evaluation: `logic.set_input_data()`
    3. Solve sheet logic
    4. Generate response object from output data: `resp_cls.from_output_data()`
    """
    default_request = request_cls()
    logic.set_input_data(default_request.to_input_cells())
    results = logic.solve()
    return resp_cls.from_output_data(results)


@pytest.mark.private_repo
@pytest.mark.parametrize(
    ("path_logic", "class_name", "description"),
    # fmt: off
    [
        (PATH_LOGIC_FILTERS, "RemakFilter", "Remak Filters vendor component"),
        (PATH_LOGIC_EXTERNAL_LINK, "ExternalLinks", "Code generation test 2"),
        (PATH_LOGIC_OLD_FILTERS_1, "FiltersOne", "Code generation test 5"),
        (PATH_LOGIC_OLD_FILTERS_2, "FiltersTwo", "Code generation test 6"),
    ],
    # fmt: on
)
def test_ads_code_generation(path_logic, class_name, description):
    """Generate ahu-data-structure interfaces and test them."""
    logic = GSheetLogic.load(path_logic)

    filename = path_logic.name.replace(".json", ".py")
    path_out = path_logic.parent / f"ads_interface_for_{filename}"
    _ = generate_attrs_interface(
        logic,
        class_name,
        path_template=PATH_ADS_TEMPLATE,
        description=description,
        path_out=path_out,
    )

    # Load the generated code
    request_cls, resp_cls = _load_dataclasses_from_generated_code(
        path_out, f"{class_name}Request", f"{class_name}"
    )

    # Test the generated code
    response = _sheet_workflow_with_dataclass(logic, request_cls, resp_cls)
    assert response.to_dict() == resp_cls().to_dict()

    # Remove generated file
    path_out.unlink()


@pytest.mark.parametrize(
    ("path_logic", "class_name", "description"),
    # fmt: off
    [
        # pytest.param(
        #     PATH_LOGIC_PUBLIC, "PublicExample", "Code generation test 1",
        #     marks=pytest.mark.xfail(reason="SyntaxError because bad labels"),
        # ),
        (PATH_LOGIC_EXTERNAL_LINK, "ExternalLinks", "Code generation test 2"),
        (PATH_LOGIC_FILTERS, "RemakFilter", "Code generation test 3"),
        # pytest.param(
        #     PATH_LOGIC_OLD_EXAMPLE, "RemakSelect", "Code generation test 4",
        #     marks=pytest.mark.xfail(reason="SyntaxError because bad labels"),
        # ),
        (PATH_LOGIC_OLD_FILTERS_1, "FiltersOne", "Code generation test 5"),
        (PATH_LOGIC_OLD_FILTERS_2, "FiltersTwo", "Code generation test 6"),
    ],
    # fmt: on
)
def test_default_code_generation(path_logic, class_name, description):
    """Generate attrs interfaces and test them."""
    logic = GSheetLogic.load(path_logic)

    filename = path_logic.name.replace(".json", ".py")
    path_out = path_logic.parent / f"interface_for_{filename}"
    _ = generate_attrs_interface(
        logic, class_name, description=description, path_out=path_out,
    )

    # Load the generated code
    request_cls, resp_cls = _load_dataclasses_from_generated_code(
        path_out, f"{class_name}Request", f"{class_name}"
    )

    # Test the generated code
    response = _sheet_workflow_with_dataclass(logic, request_cls, resp_cls)
    assert attr.asdict(response) == attr.asdict(resp_cls())

    # Remove generated file
    path_out.unlink()
