"""Test the access to the GDocs account where the Remak datatables are."""
import pytest
from envparse import env

from gs_eq_parser.credentials import authorize_access_to_gspread


@pytest.mark.download
def test_auth_access_to_sheet():
    sheet_key = env.str("GSHEET_TESTING_ACCESS_SHEET_ID")
    wks = authorize_access_to_gspread(sheet_key)

    # noinspection PyTypeChecker
    sheet2 = wks.worksheet("Sheet2")
    assert [
        ["A", "B", "C"],
        ["7", "8", "9"],
        ["10", "11", "12"],
    ] == sheet2.get_all_values()
