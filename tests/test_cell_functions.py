from datetime import datetime

import pandas as pd
import pytest

from gs_eq_parser.cellfunctions import KNOWN_FUNCS


@pytest.mark.parametrize(
    "value, factor, result",
    [
        (34.75, 0.1, 34.8),
        (34.74, 0.1, 34.7),
        (12.24, 0.1, 12.2),
        (12.25, 0.1, 12.3),
        (11, 3, 12),
        (-9, -3, -9),
        (1.5, 0.2, 1.6),
    ],
)
def test_function_mround(value, factor, result):
    func = KNOWN_FUNCS["MROUND"].function
    r = func(value, factor)
    assert r == result


@pytest.mark.parametrize(
    "result_max, result_min, result_sum, values",
    [
        (
            9,
            -4.5,
            14.5,
            pd.DataFrame([(-1, -2, -3), (1, 2, 3), (-2, -3, -4.5), (7, 8, 9)]),
        ),
        (9, -4.5, 14.5, pd.Series([-2, -3, -4.5, 7, 8, 9])),
        (9, -4.5, 14.5, (-2, -3, -4.5, 7, 8, 9)),
    ],
)
def test_function_max_min_sum(result_max, result_min, result_sum, values):
    func_max = KNOWN_FUNCS["MAX"].function
    func_min = KNOWN_FUNCS["MIN"].function
    r_max = func_max(values)
    r_min = func_min(values)
    assert r_max == result_max
    assert r_min == result_min


@pytest.mark.parametrize(
    "value, fmt, result",
    [
        (1.234, "w.ww", "1.234"),
        # This "%Y%m%d" is not the syntax formatting in GSheet yet :(
        (datetime.now(), "%Y%m%d", datetime.now().strftime("%Y%m%d")),
        (1.234, "0.00", "1.23"),
        (1.234, "$0.0", "$1.2"),
        (1.234, "^0.0 €", "^1.2 €"),
        (1.200, "#.## €", "1.2 €"),
        (7.3, "0.00", "7.30"),
        (7.3, "#.##", "7.3"),
        (7.342, "0.0", "7.3"),
        (0.342, "0.0%", "34.2%"),
        (0.06, "0.0%", "6.0%"),
        (0.06, "#.#%", "6%"),
        (1.8, "0%", "180%"),
        (7.352, "#.#", "7.4"),
        (14123, "0,000", "14,123"),
        (14000, "0,000", "14,000"),
        (14000, "#,###", "14,000"),
        (26, "$0.00", "$26.00"),
        (1000.12, "$#,###.00", "$1,000.12"),
        (31353, "#,###", "31,353"),
        (0, "00", "00"),
        (4, "00", "04"),
        (131.64, "00", "132"),
    ],
)
def test_function_text(value, fmt, result):

    func = KNOWN_FUNCS["TEXT"].function
    r = func(value, fmt)
    assert r == result
