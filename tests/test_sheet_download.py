import json
import logging
from datetime import datetime

import pytest
from envparse import env

from gs_eq_parser import GSheetLogic
from gs_eq_parser.definitions import DATE_FORMAT_OUT
from .conftest import (
    PATH_LOGIC_EXTERNAL_LINK,
    PATH_LOGIC_ISSUES,
    PATH_LOGIC_OLD_EXAMPLE,
    PATH_LOGIC_PUBLIC,
    PUBLIC_SHEET_REFERENCE_RESULTS,
)


@pytest.mark.download
def test_check_old_closed_issues():
    """
    Public sheet with open issues:
    https://docs.google.com/spreadsheets/d/
    13fJHxybpTHzzebG4ya14Nu3IBmQAwhbmz7Avidwz_tk
    /edit?usp=sharing
    """
    gs_key_sheet_issues = "13fJHxybpTHzzebG4ya14Nu3IBmQAwhbmz7Avidwz_tk"
    logic_parameters = {
        "sheet_name": "main",
        "inputs": {"main!A9:A12": "main!B9:B12"},
        "outputs": {"main!D9:D16": "main!E9:E16"},
        "sheet_key": gs_key_sheet_issues,
    }
    # Download and parse
    logic = GSheetLogic(**logic_parameters)

    # Check save
    logic.save(PATH_LOGIC_ISSUES)

    # Check re-load
    logic = GSheetLogic.load(PATH_LOGIC_ISSUES)

    # SOLVE SHEET
    results = logic.solve()
    # print(results)

    # Extract report to show input and output data
    # df_report = logic.report()
    # print(df_report)
    # show_inputs_and_outputs_from_report(df_report)

    # Check results
    good_output_data = {
        "Multiple division no parenthesis": 2.5,
        "Multiple division with parenthesis": 2.5,
        "Multiple division other parenthesis": 360,
        "what day is now": datetime.today().strftime(DATE_FORMAT_OUT),
        "vlookup with eq in index": 1728,
        "concatenate as text and add a number": "36024",
        "concat with strings 1": "36012some text",
        "concat with strings 2": "some text36012",
    }
    assert results == good_output_data

    # Change "main!B9": 15
    logic.set_input_value("main!B9", 15)
    results_2 = logic.solve()

    # Extract report to show input and output data
    _ = logic.report()

    good_output_data_2 = {
        "Multiple division no parenthesis": 2,
        "Multiple division with parenthesis": 2,
        "Multiple division other parenthesis": 450,
        "what day is now": datetime.today().strftime(DATE_FORMAT_OUT),
        "vlookup with eq in index": 3375,
        "concatenate as text and add a number": "36027",
        "concat with strings 1": "36012some text",
        "concat with strings 2": "some text36012",
    }
    assert results_2 == good_output_data_2

    errors = []

    if not results == good_output_data:
        errors.append(f"* Bad initial results: {results}")
    if not results_2 == good_output_data_2:
        errors.append(f"* Bad secondary results: {results}")

    # Check that time-dependant equation is not simplified in stored JSON
    assert "TODAY(" in PATH_LOGIC_ISSUES.read_text()

    if errors:
        raise ValueError("OPEN ISSUES:\n" + "\n".join(errors))

    # remove artifacts
    PATH_LOGIC_ISSUES.unlink()


@pytest.mark.download
@pytest.mark.parametrize(
    "sheet_params, p_reference, ref_vars_key_or_dict",
    [
        (
            {
                "sheet_name": "_PETR_DATA",
                "column_name": "VARIABLE_NAME",
                "column_value": "VALUE",
                "sheet_key": env.str("GSHEET_DEFAULT_SHEET_ID", default=None),
            },
            PATH_LOGIC_OLD_EXAMPLE,
            "example_sheet_ref_data_1",
        ),
        (
            {
                "sheet_name": "H1",
                "column_name": "VARIABLE",
                "column_value": "VALUE",
                "column_category": None,
                "sheet_key": env.str("GSHEET_TESTING_PARSER_SHEET_ID"),
            },
            PATH_LOGIC_PUBLIC,
            PUBLIC_SHEET_REFERENCE_RESULTS,
        ),
    ],
)
def test_sheet_downloader(
    sheet_old_examples, sheet_params, p_reference, ref_vars_key_or_dict
):
    _test_sheet_downloader(
        sheet_old_examples, sheet_params, p_reference, ref_vars_key_or_dict
    )


@pytest.mark.private_download
@pytest.mark.parametrize(
    "sheet_params, p_reference, ref_vars_key_or_dict",
    [
        (
            {
                "sheet_name": "outputs",
                "inputs": {"inputs!A1:A2": "inputs!B1:B2"},
                "outputs": {"outputs!A1:A3": "outputs!B1:B3"},
                "sheet_key": "1DRFcXNMAw3Oo3eGuC6PjuVPhizzr0YRiKEoMB4qA7I4",
            },
            PATH_LOGIC_EXTERNAL_LINK,
            {"outputs!B1": 1, "outputs!B2": 2, "outputs!B3": 272.5},
        ),
    ],
)
def test_sheet_downloader_private(
    sheet_old_examples, sheet_params, p_reference, ref_vars_key_or_dict
):
    _test_sheet_downloader(
        sheet_old_examples, sheet_params, p_reference, ref_vars_key_or_dict
    )


def _test_sheet_downloader(
    sheet_old_examples, sheet_params, p_reference, ref_vars_key_or_dict
):
    """Normal usage: download, solve and report, and save to disk for reuse."""
    # quick disable for unknown sheet id keys:
    if not sheet_params["sheet_key"]:
        logging.critical(
            f"Disable download test with parameters: {sheet_params}, "
            f"as key is unknown"
        )
        return

    if isinstance(ref_vars_key_or_dict, dict):
        ref_vars = ref_vars_key_or_dict
    else:
        ref_vars = sheet_old_examples[ref_vars_key_or_dict]

    postfix = f"{sheet_params['sheet_name']}"
    p_temp_save = PATH_LOGIC_PUBLIC.parent / f"sheet_test_{postfix}.json"

    # Download and creation
    logic = GSheetLogic(**sheet_params)
    logic.save(p_temp_save)

    # Evaluation
    if ref_vars is not None:
        logic.set_reference_data(ref_vars)
    _ = logic.solve()

    if ref_vars is not None:
        report, compare = logic.compare_results(reset=True)
        logging.info(compare)
        assert compare.shape[0] <= 2

    with open(p_temp_save) as f:
        generated_json = json.load(f)

    with open(str(p_reference)) as f:
        reference_json = json.load(f)

    if "PETR_DATA!A1:J158" in generated_json["raw_resources"]:
        reference_json["raw_resources"].pop("PETR_DATA!A1:J158")
        generated_json["raw_resources"].pop("PETR_DATA!A1:J158")
    assert reference_json == generated_json
    p_temp_save.unlink()
