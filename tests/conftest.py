import json
from pathlib import Path

import pytest

from gs_eq_parser.cellfunctions import cell_function_today

_PATH_TEST_EXAMPLES = Path(__file__).parent / "example_data"

# Stored sheet logic examples in JSON files
PATH_LOGIC_FILTERS = _PATH_TEST_EXAMPLES / "logic_filters.json"
PATH_LOGIC_OLD_EXAMPLE = _PATH_TEST_EXAMPLES / "logic_example_sheet.json"
PATH_LOGIC_OLD_FILTERS_1 = _PATH_TEST_EXAMPLES / "logic_old_filters_1.json"
PATH_LOGIC_OLD_FILTERS_2 = _PATH_TEST_EXAMPLES / "logic_old_filters_2.json"
PATH_LOGIC_PUBLIC = _PATH_TEST_EXAMPLES / "logic_public_sheet.json"
PATH_LOGIC_EXTERNAL_LINK = _PATH_TEST_EXAMPLES / "logic_external_link.json"
PATH_LOGIC_ISSUES = _PATH_TEST_EXAMPLES / "logic_open_issues.json"

# Date-dependent results
PUBLIC_SHEET_REFERENCE_RESULTS = {
    "H1!B3": "49",
    "H1!B4": "343",
    "H1!B5": "7",
    "H1!B6": cell_function_today(),
    "H1!B7": "27",
    "H1!B8": "20",
    "H1!B9": "60",
    "H1!B10": "3",
    "H1!B11": "120",
    "H1!B12": "3",
    "H1!B13": "#N/A",
    "H1!B14": "#N/A",
    "H1!B15": "4",
    "H1!B16": "333",
    "H1!B17": "0",
    "H1!B19": "150",
    "H1!B20": "1200",
}


@pytest.fixture
def sheet_filters_examples() -> dict:
    return json.loads(
        (_PATH_TEST_EXAMPLES / "filter_io_data_examples.json").read_text()
    )


@pytest.fixture
def sheet_old_examples() -> dict:
    return json.loads(
        (_PATH_TEST_EXAMPLES / "old_data_examples.json").read_text()
    )
