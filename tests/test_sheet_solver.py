import logging

import pytest
from envparse import env

from gs_eq_parser import GSheetLogic
from gs_eq_parser.cellfunctions import cell_function_today
from gs_eq_parser.definitions import DATE_FORMAT_OUT
from .conftest import (
    PATH_LOGIC_EXTERNAL_LINK,
    PATH_LOGIC_OLD_EXAMPLE,
    PATH_LOGIC_OLD_FILTERS_1,
    PATH_LOGIC_OLD_FILTERS_2,
    PATH_LOGIC_PUBLIC,
    PUBLIC_SHEET_REFERENCE_RESULTS,
)


def test_empty_sheet_after_no_download():
    """Credentials fail or are missing."""
    logic = GSheetLogic(
        sheet_name="DATA",
        column_name="NAME",
        column_value="VALUE",
        column_category="CATEGORY",
        sheet_key=env.str("A_KEY_THAT_DOESNT_EXIST_PROBABLY", default=None),
    )
    assert logic.solve() == {}
    with pytest.raises(ValueError):
        logic.compare_results()


def test_input_errors():
    """Bad way of setting inputs or defining parser logic."""
    with pytest.raises(ValueError) as error:
        _ = GSheetLogic(
            inputs={"S1!A1:A5": "S1!B1:B4"}, outputs={"S2!A1:A5": "S2!B1:B5"},
        )
        assert "Bad pair" in error

    logic = GSheetLogic.load(PATH_LOGIC_OLD_EXAMPLE)
    with pytest.raises(ValueError) as error:
        logic.set_input_data({"existent_input": 1})
        assert "not found" in error

    with pytest.raises(ValueError) as error:
        logic.compare_results()
        assert "Nothing to compare with" in error


def test_old_remak_sheet(sheet_old_examples):
    logic = GSheetLogic.load(PATH_LOGIC_OLD_EXAMPLE)

    old_variables = logic.solve()
    logging.info(old_variables)
    logic.set_reference_data(sheet_old_examples["example_sheet_ref_data_1"])
    report = logic.report(add_expressions=True, add_formulae=True)
    output, old_compare = logic.compare_results(reset=True)
    logging.info(old_compare)
    assert old_compare.shape[0] <= 2

    # Solve and report
    logic.set_input_value("PETR_DATA!E10", "ER25C+2.2kW")
    new_variables = logic.solve()
    logging.info(new_variables)
    _ = logic.report()
    output2, new_compare = logic.compare_results()
    logging.info(new_compare)
    assert new_compare.shape[0] > 24
    assert new_compare.shape[0] == 25

    logic.set_reference_data(sheet_old_examples["example_sheet_ref_data_2"])
    _, new_compare_to_ref = logic.compare_results()
    assert new_compare_to_ref.shape[0] < 3

    logic.set_input_value("E10", "RH31C-ZID.DG.CR+114866")
    # logic.set_input_value("PETR_DATA!E10", "RH31C-ZID.DG.CR+114866")
    new_variables_again = logic.solve()
    report3 = logic.report()
    output3, new_compare2 = logic.compare_results(reset=True)
    # print(new_compare2)
    assert new_compare2.shape[0] > 24
    assert new_compare2.shape[0] < 28

    # noinspection PyUnresolvedReferences
    assert (report3.value == report.value).all()
    # noinspection PyUnresolvedReferences
    assert (output3.value == output.value).all()

    # Variable output comparison
    assert old_variables == new_variables_again
    assert old_variables != new_variables
    assert new_variables != new_variables_again


def test_public_example():
    logic = GSheetLogic.load(PATH_LOGIC_PUBLIC)
    logic.set_reference_data(PUBLIC_SHEET_REFERENCE_RESULTS)
    old_variables = logic.solve()
    report, old_compare = logic.compare_results(reset=True)
    assert old_compare.shape[0] <= 2

    # Solve and report, changing variable 'var1', which is in "H1!B2"
    logging.info(logic.config.input_cells)
    assert logic.set_input_value("B2", 5)
    new_variables = logic.solve()
    _, new_compare = logic.compare_results(reset=True)
    assert new_compare.shape[0] > 7
    assert new_compare.shape[0] < 11
    assert new_variables == {
        "var2": 25,
        "var3": 125,
        "var4": 5,
        "var5": cell_function_today().strftime(DATE_FORMAT_OUT),
        "var6": 27,
        "var7": 16,
        "var8": 48,
        "var9": 1,
        "var10": 96,
        "var11": 1,
        "bad_formula": "#N/A",
        "var12": "#N/A",
        "var13": 4,
        "var14": 333,
        "var15": 0,
        "var17": 150,
        "var18": 1200,
    }

    assert logic.set_input_value("B2", 7)
    new_variables_again = logic.solve()
    _, new_compare_again = logic.compare_results(reset=True)
    assert new_compare_again.shape[0] > 7
    assert new_compare_again.shape[0] < 11

    # Variable output comparison
    assert old_variables == new_variables_again
    assert old_variables != new_variables
    assert new_variables != new_variables_again


def test_sheet_from_external_links_example():
    logic = GSheetLogic.load(PATH_LOGIC_EXTERNAL_LINK)

    results = logic.solve()
    assert results == {"result_a": 1, "result_b": 2, "result_c": 272.5}

    logic.set_input_data({"input_a": 2, "input_b": 3})
    results = logic.solve()
    assert results == {"result_a": 2, "result_b": 3, "result_c": 273.5}
    assert logic.get_input_data(use_labels=False) == {
        "inputs!B1": 2,
        "inputs!B2": 3,
    }

    logic.set_input_data({"inputs!B1": 3.5, "inputs!B2": 4.6})
    results = logic.solve(use_labels=False)
    assert results == {"outputs!B1": 3.5, "outputs!B2": 4.6, "outputs!B3": 275}

    logic.set_input_data({"inputs!B1": 1, "inputs!B2": 2})
    results = logic.solve(use_labels=False)
    assert results == {"outputs!B1": 1, "outputs!B2": 2, "outputs!B3": 272.5}
    assert logic.get_input_data() == {"input_a": 1, "input_b": 2}


@pytest.mark.parametrize(
    "sheet_filepath, results_ref_key, input_changes_key",
    [
        (PATH_LOGIC_OLD_FILTERS_1, "results_ref_1", None),
        (PATH_LOGIC_OLD_FILTERS_1, "results_ref_2", "input_changes_1"),
        (PATH_LOGIC_OLD_FILTERS_1, "results_ref_3", "input_changes_2"),
        (PATH_LOGIC_OLD_FILTERS_1, "results_ref_1", "input_changes_3"),
        (PATH_LOGIC_OLD_FILTERS_2, "results_ref_1_ok", None),
        (PATH_LOGIC_OLD_FILTERS_2, "results_ref_2_ok", "input_changes_1"),
        (PATH_LOGIC_OLD_FILTERS_2, "results_ref_3_ok", "input_changes_2"),
        (PATH_LOGIC_OLD_FILTERS_2, "results_ref_1_ok", "input_changes_3"),
    ],
)
def test_old_filters_logic(
    sheet_old_examples, sheet_filepath, results_ref_key, input_changes_key
):
    logic = GSheetLogic.load(sheet_filepath)
    if input_changes_key is None:
        input_changes = {}
    else:
        input_changes = sheet_old_examples[input_changes_key]

    logic.set_input_data(input_changes)

    results_ref = sheet_old_examples[results_ref_key]
    results = logic.solve()
    assert results == results_ref

    with pytest.raises(ValueError):
        logic.set_input_value("IO!A3", "something")
