import sys

from envparse import env

# env.read_envfile(Path(__file__).parents[1] / ".env")
env.read_envfile()

sys.dont_write_bytecode = True  # do not create __pycache__ on running tests
