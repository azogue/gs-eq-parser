from datetime import datetime

import numpy as np
import pandas as pd
import pytest

from gs_eq_parser.cellcoords import (
    column_index_from_string,
    get_column_letter,
    range_boundaries,
)
from gs_eq_parser.cellref import (
    CellRef,
    COL_MAX_DEFAULT,
    NULL_VALUE,
    output_cell_value,
    parse_cell_value,
    ROW_MAX_DEFAULT,
)
from gs_eq_parser.definitions import DATE_FORMAT_IN

MARK_VE = pytest.mark.xfail(raises=ValueError)
EXAMPLE_DF = pd.DataFrame(np.arange(0, 18981).reshape((-1, 19)))
EXAMPLE_CELL = CellRef.from_string("H1!A1:S999")


@pytest.mark.parametrize(
    "func_cell_coords, argument, result",
    [
        (parse_cell_value, None, None),
        (output_cell_value, None, NULL_VALUE),
        pytest.param(get_column_letter, 0, "", marks=MARK_VE),
        (get_column_letter, 1, "A"),
        (get_column_letter, 1000, "ALL"),
        (get_column_letter, 100000, "EQXD"),
        (get_column_letter, 10000000000, "AFIPYQJP"),
        pytest.param(column_index_from_string, "Ñ", "", marks=MARK_VE),
        pytest.param(range_boundaries, "A:E23", "", marks=MARK_VE),
        pytest.param(range_boundaries, "ÑI:ZA", "", marks=MARK_VE),
    ],
)
def test_cell_coordinates_helpers(func_cell_coords, argument, result):
    res = func_cell_coords(argument)
    assert res == result


@pytest.mark.parametrize(
    "raw_input, is_range, is_vector, is_vector_column,"
    "is_referenced, local_name, names, check_ref, check_len, check_repr",
    # fmt: off
    [
        ("Filtr_vlozky_data!$1:$1", True, True, False, True, None, None, "Filtr_vlozky_data!1:1", 1 * COL_MAX_DEFAULT, "<Vc Filtr_vlozky_data!1:1, S:(1, -1)>"),  # noqa
        ("Filtr_vlozky_data!3:5", True, False, False, True, None, None, "Filtr_vlozky_data!3:5", 3 * COL_MAX_DEFAULT, "<Rg Filtr_vlozky_data!3:5, S:(3, -1)>"),  # noqa
        ("Filtr_vlozky_data!B:B", True, True, True, True, None, None, "Filtr_vlozky_data!B:B", 1 * COL_MAX_DEFAULT, "<Vc Filtr_vlozky_data!B:B, S:(-1, 1)>"),  # noqa
        ("FILTR!A1:ALL1000", True, False, False, True, None, None, "FILTR!A1:ALL1000", ROW_MAX_DEFAULT * COL_MAX_DEFAULT, "<Rg FILTR!A1:ALL1000, S:(1000, 1000)>"),  # noqa
        ("'DV-ER'!$B$5:$D$8", True, False, False, True, None, None, "DV_ER!B5:D8", 12, "<Rg DV_ER!B5:D8, S:(4, 3)>"),  # noqa
        ("TAB_CNTF", True, False, False, True, None, None, "CNTF!A1:ALL1000", ROW_MAX_DEFAULT * COL_MAX_DEFAULT, "<Rg CNTF!A1:ALL1000, S:(1000, 1000)>"),  # noqa
        ("filtr_tab", True, False, False, True, None, None, "FILTR!A1:ALL1000", ROW_MAX_DEFAULT * COL_MAX_DEFAULT, "<Rg FILTR!A1:ALL1000, S:(1000, 1000)>"),  # noqa
        ('"OK"', False, False, False, False, None, None, '"OK"', 1, '<"OK" [str]>'),  # noqa
        ("5", False, False, False, False, None, None, "5", 1, "<5 [int]>"),
        pytest.param("D2!A3", False, False, False, True, "D1", ["D1", "D3"], None, None, None, marks=pytest.mark.xfail(raises=KeyError)),  # noqa
        pytest.param("ANOTHER!A3", False, False, False, True, "D1", ["D1", "D3"], None, None, None, marks=pytest.mark.xfail(raises=KeyError)),  # noqa
        ("ANOTHER!A3", False, False, False, True, "D1", ["D1", "ANOTH3R"], "ANOTH3R!A3", 1, "<Cell ANOTH3R!A3>"),  # noqa
        ("10", False, False, False, False, None, None, "10", 1, "<10 [int]>"),
        ("04", False, False, False, False, None, None, "04", 1, "<04 [str]>"),
        ('"04"', False, False, False, False, None, None, '"04"', 1, '<"04" [str]>'),  # noqa
        ("CNTF!$B$3:$H$3", True, True, False, True, None, None, "CNTF!B3:H3", 7, "<Vc CNTF!B3:H3, S:(1, 7)>"),  # noqa
        ("CNTF!$B$3:$H$30", True, False, False, True, None, None, "CNTF!B3:H30", 196, "<Rg CNTF!B3:H30, S:(28, 7)>"),  # noqa
        ("'CN TF'!$B$3:$H$30", True, False, False, True, None, ("OTHER", "CNTF", "DATA"), "CNTF!B3:H30", 196, "<Rg CNTF!B3:H30, S:(28, 7)>"),  # noqa
        # TODO fix sheet reference with spaces w/o quotes: "CN TF!$B$3:$H$30"
        # ("CN TF!$B$3:$H$30", True, False, False, True, None, ("OTHER", "CNTF", "DATA"), "CNTF!B3:H30", 196, "<Rg CNTF!B3:H30, S:(28, 7)>"),  # noqa
    ],
    # fmt: on
)
def test_cell_references(
    raw_input,
    is_range,
    is_vector,
    is_vector_column,
    is_referenced,
    local_name,
    names,
    check_ref,
    check_len,
    check_repr,
):
    c = CellRef.from_string(
        raw_input, local_sheet_name=local_name, sheet_names=names
    )
    assert c.is_range == is_range
    assert c.is_vector == is_vector
    assert c.is_vector_column == is_vector_column
    assert c.is_referenced == is_referenced
    if not c.is_referenced:
        assert c.remote_sheet is None
    assert len(c) == check_len
    assert str(c) == check_repr


def test_cells_contained_in_ranges():
    c0 = CellRef.from_string("5")
    c1 = CellRef.from_string("$E$22")
    c2 = CellRef.from_string("CNTF!$B$3:$H$3")
    c3 = CellRef.from_string("CNTF!$B$3:$H$30")
    c4 = CellRef.from_string("CNTF!$G$3:$G$30")
    assert c0 not in c1
    assert c1 not in c2
    assert c2 in c3
    assert c4 in c3
    assert c3 not in c2

    # print(list(map(len, [c1, c2, c3, c4])))
    assert len(c1) == 1
    assert len(c2) == 7
    assert len(c3) == 196
    assert len(c4) == 28

    r_big = CellRef.from_string("FAN!B5:FC28")
    r_small = CellRef.from_string("FAN!B6:B28")
    assert r_small in r_big


@pytest.mark.parametrize(
    "raw_cell, contained, shape",
    [
        ("H1!A:AB", False, (999, 19)),
        ("H1!A:B", True, (999, 2)),
        ("H1!H:H", True, (999,)),
        ("H1!10:15", True, (6, 19)),
        ("H1!2:2", True, (19,)),
    ],
)
def test_data_extraction_with_cells(raw_cell, contained, shape):
    r_small = CellRef.from_string(raw_cell)
    assert (r_small in EXAMPLE_CELL) or not contained
    extracted = EXAMPLE_CELL.extract_from_range(r_small, EXAMPLE_DF)
    assert extracted.shape == shape


def test_cell_sorting():
    raw_cells = [
        "H1!Z4",
        "H2!Z1",
        "H2!A3",
        datetime(year=2019, month=10, day=18).strftime(DATE_FORMAT_IN),
        "F2:Z5",
        "lalala",
        "D5:C15",
        "ñeñe",
        "56",
        "-34.6",
        "D2:C5",
    ]
    cells = list(map(CellRef.from_string, raw_cells))
    # print(cells)
    sorted_cells = list(sorted(cells))
    # print(sorted_cells)
    sorted_cell_keys = list(map(lambda x: x.ref, sorted_cells))
    # print(sorted_cell_keys)

    result_ref = [
        "DATA!D2:C5",
        "DATA!D5:C15",
        "DATA!F2:Z5",
        "H1!Z4",
        "H2!A3",
        "H2!Z1",
        None,
        None,
        "2019-10-18",
        "-34.6",
        "56",
    ]
    result_ref_str_repr = [
        "<Rg DATA!D2:C5, S:(4, 0)>",
        "<Rg DATA!D5:C15, S:(11, 0)>",
        "<Rg DATA!F2:Z5, S:(4, 21)>",
        "<Cell H1!Z4>",
        "<Cell H2!A3>",
        "<Cell H2!Z1>",
        "<lalala [str]>",
        "<ñeñe [str]>",
        "<2019-10-18 [date]>",
        "<-34.6 [float]>",
        "<56 [int]>",
    ]
    assert sorted_cell_keys == result_ref
    assert list(map(repr, sorted_cells)) == result_ref_str_repr
