"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

* Tree representation of expressions in formulae:

- CellExpressionTree: Recursive repr. of an expression in Excel-like syntax
- CellExprType: Enum to distinguish different kinds of implemented expressions
"""
import logging
import re
from datetime import date, datetime, timedelta
from enum import Enum
from itertools import chain
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    Optional,
    Pattern,
    Tuple,
    Union,
)

import numpy as np
import pandas as pd
from anytree import AnyNode, AsciiStyle, RenderTree

from gs_eq_parser.cellcoords import get_column_letter
from gs_eq_parser.cellfunctions import (
    apply_known_function,
    has_valid_number_of_args,
    KNOWN_FUNCS,
    KnownFunc,
    TIME_DEPENDENT_FUNCS,
)
from gs_eq_parser.cellref import (
    CellRef,
    CellValueType,
    output_cell_value,
    parse_cell_value,
    RangeValueType,
    RG_CELL_REFERENCE,
    RG_TABLE_REF,
    SPECIAL_CELLTAG,
    SPECIAL_CELLTAG_STR,
)
from gs_eq_parser.utils import (
    ensure_numeric_value,
    is_expression,
    rebuild_range_data,
)

PRIMARY_ARG_SEP = ";"
SECONDARY_ARG_SEP = ","
# set in execution when first SOMETHING(... ,|; ...) is parsed
_FIXED_DEFAULT_ARG_SEP = False
_DEFAULT_ARG_SEP = PRIMARY_ARG_SEP


def _set_arg_separator(sep):
    global _FIXED_DEFAULT_ARG_SEP
    global _DEFAULT_ARG_SEP
    if not _FIXED_DEFAULT_ARG_SEP:
        _FIXED_DEFAULT_ARG_SEP = True
        _DEFAULT_ARG_SEP = sep


# Regular expressions to parse Cell formula syntax
RG_STRINGS = re.compile(r"(?P<text>\"[^\"]*\"+)(?=[^<>:=!.]|$)")
RG_SUBEXPR_NO_PARAMS = re.compile(r"(?P<func>\w+)\((?:\s+)?\)")
RG_SUBEXPR = re.compile(r"(?P<func>\w+)?\((?P<subexpr>[^()]+)\)")
RG_COMPARATORS = re.compile(
    r"[^a-zA-Z_<>=!.]"
    r"(?P<comp>(?:(?:<=?)|(?:>=?)|(?:=)|(?:==)|(?:!=)|(?:<>)))"
    r"[^<>:=!.]"
)
RG_AND_OPERATOR = re.compile(
    r"(?:^|\d|(?:[^\w_<>]\s?))(?P<op>[&])\s?(?:[^\w_<>=!])?"
)
RG_OPERATORS = re.compile(
    r"(?:^|\d|(?:[^\w_<>^]\s?))(?P<op>[/+\-*&])\s?(?:[^\w_<>=!])?"
)
RG_POWER_OPERATOR = re.compile(
    r"(?:^|\d|(?:[^\w_<>]\s?))(?P<op>[\^])\s?(?:[^\w_<>=!])?"
)
RG_MUL_OPERS = re.compile(r"[^'\"]([/*^]|&&)[^'\"]")
RG_ADD_OPERS = re.compile(r"([+\-])")

_VTYPES = (int, float, str, bool, date, datetime, np.int_, np.float_, np.bool_)


def _try_number(value: CellValueType) -> CellValueType:
    return parse_cell_value(output_cell_value(value))


def _evaluate_addition(
    left_value: CellValueType,
    right_value: CellValueType,
    is_subs: bool = False,
):
    """
    Logic to add or substract 2 cell values.

    * If one of the terms is None (:= invalid evaluation), propagate None.
    * If one of the terms is an empty string (:= empty cell), return the other.
    * If dates are involved, try to apply day-timedeltas. If both are dates,
      use serial numbers.
    * Try to parse numbers from texts like "123" to make the numerical addition
    """
    # Quick exit if one term is None
    if left_value is None or right_value is None:
        return None

    # Ignore one of the terms if it is empty string
    if isinstance(left_value, str) and left_value in ("", '""'):
        if is_subs:
            return -ensure_numeric_value(right_value)
        return right_value
    elif isinstance(right_value, str) and right_value in ("", '""'):
        return left_value

    # Adjust addition for dates
    left_is_date = isinstance(left_value, date)
    right_is_date = isinstance(right_value, date)
    if left_is_date or right_is_date:
        if left_is_date and right_is_date:
            left_value = ensure_numeric_value(left_value)
            right_value = ensure_numeric_value(right_value)
        elif left_is_date:
            right_value = timedelta(days=right_value)
        elif right_is_date:
            left_value = timedelta(days=left_value)
        if is_subs:
            return left_value - right_value
        return left_value + right_value

    if is_subs:
        return _try_number(left_value) - _try_number(right_value)
    else:
        return _try_number(left_value) + _try_number(right_value)


class CellExprType(str, Enum):
    """Different kinds of implemented expressions."""

    # by data type
    VALUE = "X"
    VARIABLE = "V"
    DATA_RANGE = "R"
    FUNC = "F"

    # by arithmetic operation
    ADDITION = "+"
    SUBTRACTION = "-"
    MULTIPLICATION = "*"
    DIVISION = "/"
    POWER = "^"
    AND = "&"

    # by arithmetic comparison
    EQUAL = "="
    NOT_EQUAL = "!="
    NOT_EQUAL_VARIANT = "<>"
    GREATER = ">"
    EQUAL_OR_GREATER = ">="
    LOWER = "<"
    LOWER_OR_GREATER = "<="

    @property
    def is_operation(self) -> bool:
        return self.value in [
            CellExprType.ADDITION,
            CellExprType.SUBTRACTION,
            CellExprType.MULTIPLICATION,
            CellExprType.DIVISION,
            CellExprType.POWER,
            CellExprType.AND,
        ]

    @property
    def default_operation_value(self):
        assert self.is_operation
        return {
            CellExprType.ADDITION: 0,
            CellExprType.SUBTRACTION: 0,
            CellExprType.MULTIPLICATION: 0,
            CellExprType.DIVISION: 0,
            CellExprType.POWER: 0,
            CellExprType.AND: "",
        }[self.value]

    @property
    def is_comparison(self) -> bool:
        return self.value in [
            CellExprType.EQUAL,
            CellExprType.NOT_EQUAL,
            CellExprType.NOT_EQUAL_VARIANT,
            CellExprType.EQUAL_OR_GREATER,
            CellExprType.LOWER_OR_GREATER,
            CellExprType.GREATER,
            CellExprType.LOWER,
        ]

    @property
    def is_function(self) -> bool:
        return self.value == CellExprType.FUNC

    @property
    def is_cell_reference(self) -> bool:
        return self.value in [
            CellExprType.VALUE,
            CellExprType.VARIABLE,
            CellExprType.DATA_RANGE,
        ]

    @property
    def is_variable(self) -> bool:
        return self.value == CellExprType.VARIABLE

    @property
    def is_value(self) -> bool:
        return self.value == CellExprType.VALUE


def _split_in_factors(rg: Pattern, expr: str, get_first=False):
    coords_split = [s_idx for f in rg.finditer(expr) for s_idx in f.span(1)]
    raw_split = []
    idx = 0
    for s_start, s_end in enumerate(coords_split):
        raw_split.append(expr[idx:s_end])
        idx = s_end
        if s_start + 1 == len(coords_split):
            raw_split.append(expr[s_end:])

    if get_first:
        left = raw_split[0]
        right = "".join(raw_split[2:])
        return [left, right]

    if not raw_split[0]:
        raw_split.pop(0)

    if len(raw_split) % 2 == 0:
        raw_signs = raw_split[::2]
        raw_factors = raw_split[1::2]
    else:
        raw_factors = raw_split[::2]
        raw_signs = ["+"] + raw_split[1::2]

    return raw_factors, raw_signs


def _update_if_new(
    key: str, obj: "CellExpressionTree", known: Dict[str, "CellExpressionTree"]
) -> None:
    if key not in known:
        known[key] = obj


def _clean_equation_extracting_cell_strings(
    eq: str,
    sheet_name: str,
    known_subs: Dict[str, "CellExpressionTree"],
    raw_text_subs: Dict[str, str],
) -> str:
    """
    Parse strings in raw equation, substituting them by virtual expressions
    like `STRING!A1`, containing cell values with the string content.
    """
    eq = eq.lstrip("=")
    eq_reconstruct = "="
    idx_start = 0
    coords_str = [s.regs[0] for s in RG_STRINGS.finditer(eq)]
    for coords in coords_str:
        st, end = coords
        raw_cell = eq[st:end]
        parsed_cell = CellRef.from_string(
            raw_cell, local_sheet_name=sheet_name
        )

        if raw_cell in raw_text_subs:
            e_id = raw_text_subs[raw_cell]
        else:
            expr = CellExpressionTree(
                ex_type=CellExprType.VALUE, variable=parsed_cell
            )
            e_id = expr.expr_id(raw_text_subs)
            _update_if_new(e_id, expr, known_subs)

        eq_reconstruct += eq[idx_start:st]
        eq_reconstruct += e_id
        idx_start = end

    eq_reconstruct += eq[idx_start:]
    return eq_reconstruct


def _iterate_over_cell_references_in_equation(
    eq: str, local_sheet_name: str
) -> Tuple[CellRef, int, int]:
    """
    Identify cells in raw expression and parse and store each one of them,
    and rebuild the raw expression using the standardized IDs for the cells
    """
    for found in chain(
        RG_TABLE_REF.finditer(eq), RG_CELL_REFERENCE.finditer(eq)
    ):
        try:
            c2 = found.regs[2]
            if c2 == (-1, -1):
                c2 = found.regs[3]  # all-rows ref
            if c2 == (-1, -1):
                c2 = found.regs[4]  # all-columns ref

            c1 = found.regs[1]
            if c1 == (-1, -1):
                coords = c2  # simple local ref
            else:
                coords = c1[0], c2[1]

        except IndexError:  # is table ref
            coords = found.regs[1]

        raw_cell = eq[coords[0] : coords[1]]
        parsed_cell = CellRef.from_string(
            raw_cell, local_sheet_name=local_sheet_name
        )
        yield (parsed_cell, *coords)


def _clean_equation_extracting_cell_references(
    eq: str, local_sheet_name: str, known_subs: Dict[str, "CellExpressionTree"]
) -> str:
    eq = eq.lstrip("=")
    eq_reconstruct = "="
    idx_start = 0
    for parsed_cell, st, end in sorted(
        _iterate_over_cell_references_in_equation(eq, local_sheet_name),
        key=lambda x: x[1],
    ):
        eq_reconstruct += eq[idx_start:st]
        eq_reconstruct += parsed_cell.ref
        idx_start = end

        # generate expression with cell reference and store as known
        expr = CellExpressionTree(
            ex_type=(
                CellExprType.DATA_RANGE
                if parsed_cell.is_range
                else CellExprType.VARIABLE
            ),
            variable=parsed_cell,
        )
        _update_if_new(parsed_cell.ref, expr, known_subs)

    eq_reconstruct += eq[idx_start:]

    return eq_reconstruct


def _tree_dict(c: "CellExpressionTree"):
    data = c.__dict__.copy()
    if c.children:
        data["children"] = [_tree_dict(ch) for ch in c.children]
    return data


def _regen_expr(data: dict) -> "CellExpressionTree":
    ch_nodes = []
    if "children" in data:
        ch_nodes = [_regen_expr(ch) for ch in data.pop("children")]
    node = CellExpressionTree(**data)
    for ch_n in ch_nodes:
        ch_n.parent = node
    return node


def _get_node_from_known(
    key: str,
    known: Dict[str, "CellExpressionTree"],
    parent: Optional["CellExpressionTree"],
) -> "CellExpressionTree":
    known_subtree = _regen_expr(_tree_dict(known[key]))
    known_subtree.parent = parent
    known_subtree.negate = False
    known_subtree.arg_order = 0
    known_subtree.last_eval_value = None
    return known_subtree


def _eval_2_terms(
    ex_type: CellExprType,
    left_value: Union[CellValueType, RangeValueType],
    right_value: Union[CellValueType, RangeValueType],
) -> Optional[Union[CellValueType, RangeValueType]]:
    """Evaluate a 2 terms expression given left and right values."""
    if ex_type.is_operation:
        if ex_type == CellExprType.AND:
            if any(type(v) is pd.Series for v in (left_value, right_value)):
                # Assume AND operations between ranges are not mixed with
                # constant values and are always vectors.
                df_and = left_value.astype(str).str.cat(
                    right_value.astype(str)
                )
                return df_and

            # Any other combination: assume string concatenation
            res = output_cell_value(
                left_value, force_str=True
            ) + output_cell_value(right_value, force_str=True)
            return f'"{res}"'

        elif ex_type == CellExprType.ADDITION:
            return _evaluate_addition(left_value, right_value)

        # We need numbers from now on:
        if isinstance(left_value, str) or isinstance(right_value, str):
            left_value = _try_number(left_value)
            right_value = _try_number(right_value)

        if ex_type == CellExprType.SUBTRACTION:
            return _evaluate_addition(left_value, right_value, is_subs=True)
        elif ex_type == CellExprType.MULTIPLICATION:
            if (isinstance(left_value, str) and not left_value) or (
                isinstance(right_value, str) and not right_value
            ):
                logging.debug(
                    f"Null multiplication between '{left_value}' "
                    f"and '{right_value}'. "
                    f"Returning {ex_type.default_operation_value}"
                )
                return ex_type.default_operation_value
            return left_value * right_value
        elif ex_type == CellExprType.POWER:
            return pow(left_value, right_value)

        assert ex_type == CellExprType.DIVISION
        try:
            return left_value / right_value
        except ZeroDivisionError:
            logging.warning(
                f"ZeroDivisionError evaluating {left_value} / {right_value}. "
                f"Returning {ex_type.default_operation_value}"
            )
            return ex_type.default_operation_value

    elif ex_type in [
        CellExprType.EQUAL,
        CellExprType.NOT_EQUAL,
        CellExprType.NOT_EQUAL_VARIANT,
    ]:
        left_value = output_cell_value(left_value, force_str=True)
        right_value = output_cell_value(right_value, force_str=True)
        if ex_type == CellExprType.EQUAL:
            return left_value == right_value
        else:
            return left_value != right_value
    else:
        assert ex_type.is_comparison
        # Check types before arithmetical comparisons
        if type(left_value) != type(right_value):
            left_value = _try_number(left_value)
            right_value = _try_number(right_value)

        if ex_type == CellExprType.GREATER:
            return left_value > right_value
        elif ex_type == CellExprType.EQUAL_OR_GREATER:
            return left_value >= right_value
        elif ex_type == CellExprType.LOWER_OR_GREATER:
            return left_value <= right_value
        else:
            assert ex_type == CellExprType.LOWER
            return left_value < right_value


class CellExpressionTree(AnyNode):
    """Recursive representation of an expression in Excel-like syntax.

    - Each node represents an operation, or a symbolic variable, or some value.
    - The node can be negated (`x * (-1)`)
    - The node can have a known function applied (`ABS(x)`)
    - When the argument order (:= children nodes order) in the parent
    expression is needed (:= A / B != B / A), it is present in `.arg_order`
    - Finally, each node stores its last evaluated value when `.evaluate()` is
    called with variable values.
    """

    ex_type: CellExprType = CellExprType.VARIABLE
    variable: Optional[CellRef] = None
    negate: bool = False
    func_apply: Optional[str] = None
    arg_order: int = 0
    last_eval_value: Optional[CellValueType] = None

    _expr_id: str = None

    def __init__(self, parent=None, children=None, **kwargs):
        super().__init__(parent=parent, children=children, **kwargs)
        if self.ex_type.is_cell_reference and (
            not self.ex_type.is_value
            or not isinstance(self.variable.value, str)
        ):
            # Complex subexpressions and text values have other unique ID
            self._expr_id = self.variable.ref

    def __repr__(self):
        extra = "(-)" if self.negate else ""
        if self.ex_type.is_function:
            type_repr = f"{extra}{self.func_apply}({len(self.children)})"
        else:
            type_repr = f"{extra}{self.ex_type.name}"
        str_repr = f"Expr[{type_repr}]"

        if self.ex_type == CellExprType.DATA_RANGE:
            str_repr += f": R={self.variable}"
        elif self.ex_type.is_value:
            str_repr += f"; V={self.variable}"
        elif self.ex_type.is_variable:
            str_repr += f"; cell: {self.variable}"

        if (
            self.last_eval_value is not None
            and self.ex_type != CellExprType.VALUE
        ):
            if type(self.last_eval_value) in _VTYPES:
                str_repr += f" ==> {self.last_eval_value}"
            else:
                # noinspection PyUnresolvedReferences
                str_repr += f" --> shape: {self.last_eval_value.shape}"

        return str_repr

    def __hash__(self):
        return hash(self._expr_id)

    @staticmethod
    def _sort_subexprs(items):
        return list(sorted(items, key=lambda x: x.arg_order))

    def _children_args(self) -> List["CellExpressionTree"]:
        return list(self._sort_subexprs(self.children))

    def __eq__(self, other: "CellExpressionTree"):
        if (
            type(self) != type(other)
            or self.ex_type != other.ex_type
            or self.variable != other.variable
            or self.func_apply != other.func_apply
            or len(self.children) != len(other.children)
            # disposition / temporal fields, don't apply to equal comparison
            # or self.negate != other.negate
            # or self.arg_order != other.arg_order
            # or self.last_eval_value != other.last_eval_value
        ):
            return False
        for ch, other_ch in zip(self._children_args(), other._children_args()):
            if ch != other_ch:
                return False
        return True

    def render(self, return_string=False):
        """Pretty representation of the formula tree."""
        kwargs = {"style": AsciiStyle(), "childiter": self._sort_subexprs}
        if return_string:
            return str(RenderTree(self, **kwargs))
        print(RenderTree(self, **kwargs))  # pragma: no cover

    @classmethod
    def from_value(
        cls,
        raw_value: CellValueType,
        parent: Optional["CellExpressionTree"] = None,
    ):
        """Create a simple CellExpressionTree containing a value."""
        parsed_value = parse_cell_value(raw_value)
        cell_value = CellRef(is_referenced=False, value=parsed_value)
        return cls(
            ex_type=CellExprType.VALUE,
            variable=cell_value,
            last_eval_value=parsed_value,
            parent=parent,
        )

    @classmethod
    def _identify_cells_in_expr(
        cls,
        eq: str,
        sheet_name: str,
        known_subs: Dict[str, "CellExpressionTree"],
        raw_text_subs: Dict[str, str],
    ) -> str:
        """
        Identify cells in raw expression and parse and store each one of them,
        and rebuild the raw expression using the standardized IDs for the cells
        """
        # Extract strings from equation
        eq_parsed_strings = _clean_equation_extracting_cell_strings(
            eq, sheet_name, known_subs, raw_text_subs
        )

        # Extract cell references from equation
        return _clean_equation_extracting_cell_references(
            eq_parsed_strings, sheet_name, known_subs
        )

    @classmethod
    def parse(
        cls,
        eq: str,
        local_sheet_name: str,
        known_subs: Dict[str, "CellExpressionTree"] = None,
        raw_text_subs: Dict[str, str] = None,
        parent: Optional["CellExpressionTree"] = None,
        sheet_names: Optional[Iterable[str]] = None,
    ):
        """Create a CellExpressionTree from raw equation string."""
        if known_subs is None:
            known_subs = {}
        if raw_text_subs is None:
            raw_text_subs = {}

        # Identify all references for cells and ranges in expression
        eq = cls._identify_cells_in_expr(
            eq, local_sheet_name, known_subs, raw_text_subs=raw_text_subs
        )

        # Run recursive expression parsing
        return cls._rec_parse(
            eq.lstrip("="),
            local_sheet_name,
            known_subs,
            raw_text_subs=raw_text_subs,
            parent=parent,
            sheet_names=sheet_names,
        )

    @classmethod
    def _append_child_nodes_for_parent(
        cls,
        parent: "CellExpressionTree",
        raw_child_terms: Iterable[str],
        sheet_name: str,
        known: Dict[str, "CellExpressionTree"],
        raw_text_subs: Dict[str, str],
        sheet_names: Optional[Iterable[str]] = None,
    ):
        for i, raw_term in enumerate(raw_child_terms):
            ch = cls._rec_parse(
                raw_term,
                sheet_name,
                known,
                raw_text_subs=raw_text_subs,
                parent=parent,
                sheet_names=sheet_names,
            )
            ch.arg_order = i
            _update_if_new(ch.expr_id(raw_text_subs), ch, known)

    @classmethod
    def _create_negated_node(
        cls,
        op: CellExprType,
        raw_right: str,
        sheet_name: str,
        known: Dict[str, "CellExpressionTree"],
        raw_text_subs: Dict[str, str],
        parent: Optional["CellExpressionTree"] = None,
        sheet_names: Optional[Iterable[str]] = None,
    ):
        """
        Make 'negated' nodes when a 2 terms decomposition like
         `"" + "-" + expr` is found, where there is no left term,
         but the right term is simply negated: `-(expr)`.
        """
        assert op == CellExprType.SUBTRACTION
        negated_node = cls._rec_parse(
            raw_right,
            sheet_name,
            known,
            raw_text_subs=raw_text_subs,
            sheet_names=sheet_names,
        )
        if negated_node.ex_type.is_value:
            return cls.from_value(-negated_node.variable.value, parent)

        negated_node.negate = True
        negated_node.parent = parent
        return negated_node

    @classmethod
    def _create_operator_node_with_childs(
        cls,
        op: CellExprType,
        raw_left: str,
        raw_right: str,
        sheet_name: str,
        known: Dict[str, "CellExpressionTree"],
        raw_text_subs: Dict[str, str],
        parent: Optional["CellExpressionTree"] = None,
        sheet_names: Optional[Iterable[str]] = None,
    ):
        assert op.is_operation or op.is_comparison

        if not raw_left.strip() and op == CellExprType.SUBTRACTION:
            # It's just a negation
            return cls._create_negated_node(
                op,
                raw_right,
                sheet_name,
                known,
                raw_text_subs,
                parent,
                sheet_names,
            )

        pnode = cls(ex_type=op)
        cls._append_child_nodes_for_parent(
            pnode,
            [raw_left, raw_right],
            sheet_name,
            known,
            raw_text_subs=raw_text_subs,
            sheet_names=sheet_names,
        )
        pnode.parent = parent
        return pnode

    @classmethod
    def _rec_parse(
        cls,
        eq: str,
        sheet_name: str,
        known: Dict[str, "CellExpressionTree"],
        raw_text_subs: Dict[str, str],
        parent: Optional["CellExpressionTree"] = None,
        sheet_names: Optional[Iterable[str]] = None,
    ):
        """Recursive constructor of equation terms."""
        # Initial cleaning of sub-expression
        eq = eq.strip()

        if eq in known:
            return _get_node_from_known(eq, known, parent)

        elif eq in raw_text_subs:
            special_eq = raw_text_subs[eq]
            return _get_node_from_known(special_eq, known, parent)

        # Look for special function calls without parameters
        search_sub_expr = RG_SUBEXPR_NO_PARAMS.search(eq)
        if search_sub_expr is not None:
            found = search_sub_expr.groupdict()
            func_name = found["func"].strip()
            simple_func_node = cls(ex_type=CellExprType.FUNC)
            simple_func_node.func_apply = func_name.upper()
            _ = has_valid_number_of_args(func_name, 0, eq)
            key = simple_func_node.expr_id(raw_text_subs)
            _update_if_new(key, simple_func_node, known)
            return cls._rec_parse(
                RG_SUBEXPR_NO_PARAMS.sub(key, eq, count=1),
                sheet_name,
                known,
                raw_text_subs=raw_text_subs,
                parent=parent,
                sheet_names=sheet_names,
            )

        # Look for subexpressions or function calls
        search_sub_expr = RG_SUBEXPR.search(eq)
        if search_sub_expr is not None:
            found = search_sub_expr.groupdict()
            if found["func"]:
                func_name = found["func"].strip()
                if PRIMARY_ARG_SEP in found["subexpr"]:
                    _set_arg_separator(PRIMARY_ARG_SEP)
                    args_func = found["subexpr"].split(PRIMARY_ARG_SEP)
                else:
                    _set_arg_separator(SECONDARY_ARG_SEP)
                    args_func = found["subexpr"].split(SECONDARY_ARG_SEP)
                subexpr_node = cls(ex_type=CellExprType.FUNC)
                subexpr_node.func_apply = func_name.upper()
                cls._append_child_nodes_for_parent(
                    subexpr_node,
                    args_func,
                    sheet_name,
                    known,
                    raw_text_subs=raw_text_subs,
                    sheet_names=sheet_names,
                )
                _ = has_valid_number_of_args(func_name, len(args_func), eq)
            else:
                subexpr_node = cls._rec_parse(
                    found["subexpr"],
                    sheet_name,
                    known,
                    raw_text_subs=raw_text_subs,
                    sheet_names=sheet_names,
                )
            key = subexpr_node.expr_id(raw_text_subs)
            _update_if_new(key, subexpr_node, known)
            return cls._rec_parse(
                RG_SUBEXPR.sub(key, eq, count=1),
                sheet_name,
                known,
                raw_text_subs=raw_text_subs,
                parent=parent,
                sheet_names=sheet_names,
            )

        # Search for comparators '>', '<', '='
        search_comp_data = RG_COMPARATORS.search(eq)
        if search_comp_data is not None:
            comp = search_comp_data.groupdict()["comp"]
            first, second = eq.split(comp, maxsplit=1)
            return cls._create_operator_node_with_childs(
                CellExprType(comp),
                first,
                second,
                sheet_name,
                known,
                raw_text_subs=raw_text_subs,
                parent=parent,
                sheet_names=sheet_names,
            )

        # Search for '&' concatenation operator
        found_and_ops = RG_AND_OPERATOR.findall(eq)
        if len(found_and_ops) > 0:
            # Multiple factor concatenation
            pnode = cls(ex_type=CellExprType.AND, parent=parent)
            for factor in eq.split("&"):
                ch = cls._rec_parse(
                    factor,
                    sheet_name,
                    known,
                    raw_text_subs=raw_text_subs,
                    sheet_names=sheet_names,
                )
                ch.parent = pnode
                _update_if_new(ch.expr_id(raw_text_subs), ch, known)
            return pnode

        # Search for Arithmetic operators (+, -, *, /)
        found_ops = RG_OPERATORS.findall(eq)
        if not found_ops:
            # Try power operation x^y; x^y^z computes as x^(y^z)
            if RG_POWER_OPERATOR.findall(eq):
                raw_left, raw_right = _split_in_factors(
                    RG_POWER_OPERATOR, eq, get_first=True
                )
                return cls._create_operator_node_with_childs(
                    CellExprType.POWER,
                    raw_left,
                    raw_right,
                    sheet_name,
                    known,
                    raw_text_subs=raw_text_subs,
                    parent=parent,
                    sheet_names=sheet_names,
                )

            # Found simple cell value
            var = CellRef.from_string(eq, local_sheet_name=sheet_name)
            # All variables have been parsed in this level, so only values
            assert not var.is_referenced
            return cls(
                ex_type=CellExprType.VALUE,
                variable=var,
                parent=parent,
                last_eval_value=var.value,
            )

        elif len(found_ops) > 1 and len(RG_ADD_OPERS.findall(eq)) > 1:
            # Multiple factor addition/subtraction
            raw_factors, raw_signs = _split_in_factors(RG_ADD_OPERS, eq)
            pnode = cls(ex_type=CellExprType.ADDITION, parent=parent)
            for factor, raw_sign in zip(raw_factors, raw_signs):
                ch = cls._rec_parse(
                    factor,
                    sheet_name,
                    known,
                    raw_text_subs=raw_text_subs,
                    sheet_names=sheet_names,
                )
                ch.negate = CellExprType(raw_sign) == CellExprType.SUBTRACTION
                if ch.negate and ch.ex_type.is_value:
                    ch = cls.from_value(-ch.variable.value)
                ch.parent = pnode
                _update_if_new(ch.expr_id(raw_text_subs), ch, known)
            return pnode

        # Left / right normal operation
        if len(found_ops) == 1:
            op = found_ops[0]
            raw_left, raw_right = eq.split(op, maxsplit=1)
            if not raw_left.strip() and op == CellExprType.SUBTRACTION.value:
                # It's just a negation
                return cls._create_negated_node(
                    CellExprType(op),
                    raw_right,
                    sheet_name,
                    known,
                    raw_text_subs,
                    parent,
                    sheet_names,
                )
        elif (
            RG_MUL_OPERS.findall(eq) or RG_POWER_OPERATOR.findall(eq)
        ) and RG_ADD_OPERS.findall(eq):
            # parse addition factors in pairs
            raw_left, op, raw_right = RG_ADD_OPERS.split(eq, maxsplit=1)
        else:
            # Split expression in 2 terms, isolating the one in the right
            # to preserve good behaviour in divisions, as (1/2)/3 != (1/(2/3))
            op = found_ops[-1]
            args = eq.split(op)
            raw_left = op.join(args[:-1])
            raw_right = args[-1]

        return cls._create_operator_node_with_childs(
            CellExprType(op),
            raw_left,
            raw_right,
            sheet_name,
            known,
            raw_text_subs=raw_text_subs,
            parent=parent,
            sheet_names=sheet_names,
        )

    def expr_id(
        self, raw_text_substitutions: Optional[Dict[str, str]] = None
    ) -> Optional[str]:
        """Use to generate a new variable name for complex expressions."""
        if self._expr_id is not None:
            return self._expr_id

        # Generation of new special cell for the subexpression
        if raw_text_substitutions is None:
            return None

        def _get_coord(x):
            return get_column_letter(x // 999 + 1), x % 999 + 1

        if self.ex_type.is_value:
            tag = SPECIAL_CELLTAG_STR
            raw_text = self.variable.value
        else:
            tag = SPECIAL_CELLTAG
            raw_text = self.text

        if raw_text not in raw_text_substitutions:
            key = "{0}!{1}{2}".format(
                tag, *_get_coord(len(raw_text_substitutions))
            )
            raw_text_substitutions[raw_text] = key
            self._expr_id = key
        else:
            self._expr_id = raw_text_substitutions[raw_text]

        return self._expr_id

    @property
    def dependent_variables(self) -> List["CellExpressionTree"]:
        """Extract dependent variables from expression."""
        if self.ex_type.is_cell_reference:
            return []

        return list(
            set(filter(lambda x: x.ex_type.is_variable, self.descendants))
        )

    @property
    def is_time_dependent_function(self) -> bool:
        """Check if current root expression is a time-dependent function."""
        return (
            self.ex_type.is_function
            and self.func_apply in TIME_DEPENDENT_FUNCS
        )

    @property
    def is_time_dependent_expression(self) -> bool:
        """Check if formulae includes any time-dependent function."""
        if self.ex_type.is_cell_reference:
            return False

        return self.is_time_dependent_function or any(
            map(lambda c: c.is_time_dependent_function, self.descendants)
        )

    @property
    def dependent_ranges(self) -> List["CellExpressionTree"]:
        """Extract dependent cell ranges from expression."""
        if self.ex_type.is_cell_reference:
            return []

        return list(
            set(
                filter(
                    lambda x: x.ex_type == CellExprType.DATA_RANGE,
                    self.descendants,
                )
            )
        )

    def _get_function(self) -> Optional[KnownFunc]:
        assert self.func_apply is not None
        try:
            return KNOWN_FUNCS[self.func_apply]
        except KeyError as bad_f_key:
            # Conservative behaviour: return None as #N/A
            logging.warning(f"Func_'{bad_f_key}' not implemented!")
            self.last_eval_value = None
            return None

    def _apply_func(
        self,
        getter_known_value: Callable,
        known_values: Dict[str, CellValueType],
        *args,
    ):
        if len(args) == 1 and args[0] is None:
            logging.debug(f"Can't apply, value is None for {self.text}")
            return None

        if self.func_apply:
            func = self._get_function()
            if func is None:
                return None

            try:
                value = apply_known_function(func, *args)
            except (ValueError, TypeError) as exc:
                # Conservative behaviour: return None as #N/A
                logging.debug(f"Error in evaluation: {exc}")
                self.last_eval_value = None
                return None

            while is_expression(value):  # nested CellExpressionTree
                value = value.evaluate(known_values, getter_known_value)
        else:
            value = args[0]

        if self.negate:
            value = ensure_numeric_value(parse_cell_value(value))
            value *= -1
        self.last_eval_value = value
        return value

    def _eval_children(
        self,
        known_values: Dict[str, CellValueType],
        getter_known_value: Callable,
    ) -> List[CellValueType]:
        child_evals = []
        for ch in self._children_args():
            child_evals.append(ch.evaluate(known_values, getter_known_value))
        return child_evals

    def evaluate(
        self,
        known_values: Dict[str, CellValueType] = None,
        getter_known_value: Optional[Callable] = None,
    ) -> Optional[CellValueType]:
        """Solve expression using an external dict of values for variables."""
        if known_values is None:
            known_values = {}

        if getter_known_value is None:  # pragma: no cover

            def _local_getter(x, *_args):
                return known_values[x]

            getter_known_value = _local_getter

        if self.ex_type.is_value:
            return self.variable.value

        _key_id = self.expr_id()
        if _key_id in known_values:
            result = known_values[_key_id]
            if self.negate and result is not None:
                result = -ensure_numeric_value(result)
            self.last_eval_value = result
            return result

        elif self.ex_type.is_variable:
            result = getter_known_value(self.variable)
            return self._apply_func(getter_known_value, known_values, result)

        elif self.ex_type.is_cell_reference:  # RANGE
            result = getter_known_value(self.variable)
            values_flat = result.values.ravel()
            arr_bool_is_expr = np.array(list(map(is_expression, values_flat)))
            if np.count_nonzero(arr_bool_is_expr):
                for pos in np.where(arr_bool_is_expr)[0]:
                    result_ij = values_flat[pos].evaluate(
                        known_values, getter_known_value
                    )
                    values_flat[pos] = result_ij
                logging.debug(
                    f"RANGE {self} was going to fail!, "
                    f"it has {np.count_nonzero(arr_bool_is_expr)} "
                    f"expressions inside."
                )
                result = rebuild_range_data(values_flat, result)

        elif self.ex_type.is_function:
            result = self._apply_func(
                getter_known_value,
                known_values,
                *self._eval_children(known_values, getter_known_value),
            )

        elif len(self.children) > 2:
            solutions = self._eval_children(known_values, getter_known_value)
            if any(isinstance(x, date) for x in solutions):
                # Operations with multiple terms and dates mixed in -> numeric
                solutions = list(map(ensure_numeric_value, solutions))

            assert self.ex_type in [CellExprType.AND, CellExprType.ADDITION]
            # treat as 2 term expressions
            left = solutions[0]
            for right in solutions[1:]:
                left = _eval_2_terms(self.ex_type, left, right)
            return self._apply_func(getter_known_value, known_values, left)

        else:  # 2 terms expressions for now on:
            left, right = self._eval_children(known_values, getter_known_value)
            try:
                result = self._apply_func(
                    getter_known_value,
                    known_values,
                    _eval_2_terms(self.ex_type, left, right),
                )
            except (ValueError, TypeError) as exc:
                if self.ex_type.is_operation:
                    result = self.ex_type.default_operation_value
                else:
                    result = None
                logging.debug(
                    f"{exc.__class__.__name__} trying to evaluate "
                    f"a {self.ex_type.name} operation between "
                    f"{left} [{type(left).__name__}] and "
                    f"{right} [{type(right).__name__}] in {self} "
                    f"Error: {exc}. Returning {result}."
                )

        if _key_id is not None:
            if self.negate:
                known_values[_key_id] = -ensure_numeric_value(
                    parse_cell_value(result)
                )
            else:
                known_values[_key_id] = result

        return result

    @property
    def text(self) -> str:
        """Regenerate text equation from tree nodes."""

        def _2_term_expr(left, right, operator: CellExprType) -> str:
            if not left.negate and (
                left.ex_type.is_cell_reference or left.ex_type.is_function
            ):
                sub_text = f"{left.text}"
            else:
                sub_text = f"({left.text})"
            sub_text += f"{operator.value}"
            if not right.negate and (
                right.ex_type.is_cell_reference or right.ex_type.is_function
            ):
                sub_text += f"{right.text}"
            else:
                sub_text += f"({right.text})"

            return sub_text

        def _negate_with_grouping(negate: bool, sub_text: str) -> str:
            if negate:
                return f"-({sub_text})"
            return sub_text

        if self.ex_type.is_value:
            v = self.variable.value
            if isinstance(v, str):
                return v
            return self.variable.ref
        elif self.ex_type.is_cell_reference:
            if self.negate:
                return f"-{self.variable.ref}"
            return self.variable.ref
        elif self.ex_type == CellExprType.ADDITION:
            text = ""
            for i, ch in enumerate(self._children_args()):
                if (
                    i > 0
                    and (
                        ch.ex_type != CellExprType.VALUE
                        or ch.variable.value >= 0
                    )
                    and not ch.negate
                ):
                    text += f"+{ch.text}"
                else:
                    text += f"{ch.text}"
            return _negate_with_grouping(self.negate, text)
        elif self.ex_type == CellExprType.AND:
            text = ""
            for i, ch in enumerate(self._children_args()):
                if i > 0:
                    text += f"&{ch.text}"
                else:
                    text += f"{ch.text}"
            return _negate_with_grouping(self.negate, text)
        elif self.ex_type.is_function:
            text = SECONDARY_ARG_SEP.join(
                map(lambda x: x.text, self._children_args())
            )
            if self.negate:
                return f"-{self.func_apply}({text})"
            return f"{self.func_apply}({text})"
        return _negate_with_grouping(
            self.negate,
            _2_term_expr(self.children[0], self.children[1], self.ex_type),
        )
