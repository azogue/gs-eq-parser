import logging
from datetime import date, datetime
from functools import wraps
from time import time
from typing import Callable, Dict, Iterable, Optional, Tuple, Union

import numpy as np
import pandas as pd

from gs_eq_parser.cellcoords import get_column_letter
from gs_eq_parser.cellref import CellRef
from gs_eq_parser.definitions import (
    CellIOValueType,
    CellValueType,
    RangeValueType,
)

# For conversions from dates to numbers, the 'one' is 1 January 1900:
INIT_EXCEL_DATETIME = datetime(year=1899, month=12, day=31)


def _timer_wrap(
    func: Callable,
    msg_log: str,
    level: int,
    minimum_time=None,
    *args,
    **kwargs,
):
    tic = time()
    out = func(*args, **kwargs)
    took = time() - tic
    msg = f"-> ** {func.__name__}:{msg_log} took {took:.3f} s **"
    if minimum_time is not None and took < minimum_time:
        return out
    logging.log(level=level, msg=msg)
    return out


def timeit(level: int = logging.INFO, minimum_time=None):
    """Wrap an instance method to log the execution time of a method call."""

    def _timeit(func: Callable):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return _timer_wrap(func, "", level, minimum_time, *args, **kwargs)

        return wrapper

    return _timeit


def make_cell_ref(row: int, col: int, sheet_name: Optional[str] = None) -> str:
    """Generate cell key given internal row and column, and sheet name."""
    if sheet_name is None:
        return f"{get_column_letter(col + 1)}{row + 1}"
    return f"{sheet_name}!{get_column_letter(col + 1)}{row + 1}"


def is_expression(x) -> bool:
    """
    Check if an object is a CellExpressionTree by trying to get the `.ex_type`,

    This is just an internal optimization for an operation repeated thousands
    of times in sheet evaluation, so use with care.

    Little advantages over hasattr or isinstance are substantial that way.

    # try:
    #     _ = x.ex_type
    #     return True
    # except AttributeError:
    #     return False
    # return hasattr(x, "ex_type")
    # return type(x).__name__ == "CellExpressionTree"
    """
    return getattr(x, "ex_type", None) is not None


def filter_formula(x):
    """Mask equations (string fields starting with '=')."""
    return x if isinstance(x, str) and x.startswith("=") else None


def is_iterable(x):
    """Check an object for _iterability_, excluding strings."""
    return isinstance(x, Iterable) and not isinstance(x, str)


def get_resource_from_cell(
    cell: CellRef, resources: Dict[str, Tuple[CellRef, RangeValueType]]
):
    """Extract a subset (cell, vector, matrix) from given dict of resources."""
    rsc_cell, rsc_data = resources[cell.remote_sheet]
    return rsc_cell.extract_from_range(cell, rsc_data)


def ensure_referenced_cell(
    cell: Union[str, CellRef], sheet_name: str = "DATA"
) -> CellRef:
    """Provide a CellRef object given a cell reference as a string."""
    if type(cell) is CellRef:  # pragma: no cover
        c = cell
    else:
        c = CellRef.from_string(cell, local_sheet_name=sheet_name)
    assert c.is_referenced
    return c


def ensure_numeric_value(value: CellValueType) -> CellIOValueType:
    """
    Used for conversion of a datetime internal object to serial number,
    as it is done in Excel/GSheet.
    """
    if type(value) in (date, datetime):
        if isinstance(value, datetime):
            return (value - INIT_EXCEL_DATETIME).total_seconds() / (
                3600 * 24
            ) + 1
        return (value - INIT_EXCEL_DATETIME.date()).days + 1
    return value


def rebuild_range_data(
    values: np.array, original: Union[pd.DataFrame, pd.Series]
):
    """Create a pandas object with new numpy array of values."""
    data = values.reshape(original.shape)
    if len(original.shape) == 1:  # pd.Series
        return pd.Series(data, index=original.index, name=original.name)

    return pd.DataFrame(data, index=original.index, columns=original.columns)
