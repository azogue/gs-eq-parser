"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- Sheet configuration, formatting, etc.
 (to adapt to different countries/languages)
"""
from itertools import chain
from typing import Callable, Dict, Iterator, List, Tuple

import attr
import pandas as pd

from gs_eq_parser.cellcoords import get_column_letter
from gs_eq_parser.cellexpression import CellExpressionTree
from gs_eq_parser.cellref import CellRef, expand_cell_vector_into_cells


def _make_cell_ranges(raw_ranges: dict, sheet_name: str):
    ranges_dict = {
        CellRef.from_string(
            k, local_sheet_name=sheet_name
        ): CellRef.from_string(v, local_sheet_name=sheet_name)
        for k, v in raw_ranges.items()
    }
    # Check integrity:
    for k, v in ranges_dict.items():
        if k.shape != v.shape:
            raise ValueError(
                f"Bad pair of ranges {k.ref} -> {v.ref}: shapes don't match"
            )
    return ranges_dict


def _ref(x: CellRef) -> str:
    return x.ref


def _double_ref(x: Tuple[CellRef, CellRef]) -> Tuple[str, str]:
    return _ref(x[0]), _ref(x[1])


@attr.s(auto_attribs=True)
class SheetConfig:
    sheet_name: str = ""

    col_name: str = ""
    col_value: str = ""

    # Generic definition of cells considered inputs and outputs
    use_generic_ranges: bool = False
    inputs: Dict[CellRef, CellRef] = attr.ib(factory=dict)
    outputs: Dict[CellRef, CellRef] = attr.ib(factory=dict)

    _input_cells: List[CellRef] = attr.ib(factory=list)
    _input_label_cells: List[CellRef] = attr.ib(factory=list)
    _input_label_values: List[str] = attr.ib(factory=list)

    _output_cells: List[CellRef] = attr.ib(factory=list)
    _output_label_cells: List[CellRef] = attr.ib(factory=list)
    _output_label_values: List[str] = attr.ib(factory=list)

    def __attrs_post_init__(self):
        # Expand input and output cells
        if self.use_generic_ranges:
            self._input_cells = [
                c
                for rg_c in self.inputs.values()
                for c in expand_cell_vector_into_cells(rg_c)
            ]
            self._input_label_cells = [
                c
                for rg_c in self.inputs.keys()
                for c in expand_cell_vector_into_cells(rg_c)
            ]
            self._output_cells = [
                c
                for rg_c in self.outputs.values()
                for c in expand_cell_vector_into_cells(rg_c)
            ]
            self._output_label_cells = [
                c
                for rg_c in self.outputs.keys()
                for c in expand_cell_vector_into_cells(rg_c)
            ]

    @classmethod
    def from_user_data(cls, **data):
        """
        Create configuration from stored JSON or base definition.

        It can be defined by 2 column names in one sheet,
        using one column for labels and the other for input/output cells,
        or by dicts for inputs cells or ranges and output cells or ranges,
        with label cells as keys and input/output cells as values
        of those dicts.
        """
        if "inputs" not in data or data["inputs"] is None:
            # simple column_name / column_var pattern
            simple_data = {
                "use_generic_ranges": False,
                "sheet_name": data.pop("sheet_name"),
                "col_name": data.pop("col_name"),
                "col_value": data.pop("col_value"),
            }
            return cls(**simple_data)

        raw_inputs = data.pop("inputs")
        raw_outputs = data.pop("outputs")
        sheet_name = data.pop("sheet_name")
        generic_data = {
            "use_generic_ranges": True,
            "sheet_name": sheet_name,
            "inputs": _make_cell_ranges(raw_inputs, sheet_name),
            "outputs": _make_cell_ranges(raw_outputs, sheet_name),
        }
        return cls(**generic_data)

    @classmethod
    def from_column_definition(
        cls, other: "SheetConfig", main_rsc_df: pd.DataFrame
    ):
        """Generate configuration from alternate definition of input/output."""
        # Set inputs / outputs and their labels by column:
        sheet_name = other.sheet_name
        header = pd.Index(main_rsc_df.iloc[0])
        idx_value = header.get_loc(other.col_value)
        letter_value = get_column_letter(idx_value + 1)
        idx_name = header.get_loc(other.col_name)
        letter_name = get_column_letter(idx_name + 1)

        df_io = main_rsc_df.loc[1:, [idx_name, idx_value]]
        inputs, outputs = {}, {}
        range_row_start = 2  # header is excluded
        range_is_input = True
        mask_n = f"{sheet_name}!{letter_name}{{0}}:{letter_name}{{1}}"
        mask_v = f"{sheet_name}!{letter_value}{{0}}:{letter_value}{{1}}"

        def _make_range(start, end) -> dict:
            k_l = mask_n.format(start, end)
            k_i = mask_v.format(start, end)
            if range_row_start == end:
                k_l = k_l.split(":")[0]
                k_i = k_i.split(":")[0]
            return {k_l: k_i}

        idx_row = 0
        for idx_row, row in df_io.iterrows():
            is_expr = isinstance(row.loc[idx_value], CellExpressionTree)
            if (is_expr and range_is_input) or (
                not is_expr and not range_is_input
            ):
                group_io = inputs if range_is_input else outputs
                group_io.update(_make_range(range_row_start, idx_row))
                range_is_input = not range_is_input
                range_row_start = idx_row + 1
        else:
            if idx_row:
                # close last range
                group_io = inputs if range_is_input else outputs
                group_io.update(_make_range(range_row_start, idx_row + 1))

        generic_data = {
            "use_generic_ranges": True,
            "sheet_name": sheet_name,
            "inputs": _make_cell_ranges(inputs, sheet_name),
            "outputs": _make_cell_ranges(outputs, sheet_name),
        }
        return cls(**generic_data)

    def to_dict(self) -> dict:
        """Use this instead of attr.asdict(self) to ignore internals."""
        return {
            "sheet_name": self.sheet_name,
            "use_generic_ranges": self.use_generic_ranges,
            "inputs": dict(map(_double_ref, self.inputs.items())),
            "outputs": dict(map(_double_ref, self.outputs.items())),
        }

    def set_labels(self, rsc_value_getter: Callable):
        def _set_labels(label_cells, label_values):
            for cell in label_cells:
                label = rsc_value_getter(cell)
                label_values.append(label)

        _set_labels(self._input_label_cells, self._input_label_values)
        _set_labels(self._output_label_cells, self._output_label_values)

    @property
    def input_keys(self) -> Iterator:
        """Iterator over input cell keys."""
        return map(_ref, self._input_cells)

    @property
    def equation_keys(self) -> Iterator:
        """Iterator over output cell keys."""
        return map(_ref, self._output_cells)

    @property
    def inputs_dict(self) -> Dict[str, str]:
        """Get a dict with input cell keys and input labels as values."""
        return dict(zip(self.input_keys, self._input_label_values))

    @property
    def equations_dict(self) -> Dict[str, str]:
        """Get a dict with output cell keys and output labels as values."""
        return dict(zip(self.equation_keys, self._output_label_values))

    @property
    def input_cells(self) -> List[CellRef]:
        """Get a list of input CellRef objects."""
        return self._input_cells

    @property
    def output_cells(self) -> List[CellRef]:
        """Get a list of output CellRef objects."""
        return self._output_cells

    @property
    def output_labels(self) -> List[str]:
        return self._output_label_values

    @property
    def forbidden_keys(self) -> List[str]:
        """Return list of forbidden cell keys (:= equations or inputs)."""
        return list(chain(self.input_keys, self.equation_keys))

    @property
    def forbidden_coords(self) -> List[Tuple[str, int, int]]:
        """
        Return list of forbidden cell coordinates (:= equations or inputs),
        containing:
        - sheet name
        - internal coordinates (row, column)
        """
        return list(
            map(
                lambda x: (x.remote_sheet, x.coords[0], x.coords[1]),
                chain(self._input_cells, self._output_cells),
            )
        )
