"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- Sheet configuration, formatting, etc.
 (to adapt to different countries/languages)
"""
from datetime import date
from typing import Union

import pandas as pd
from envparse import env

DATE_FORMAT_IN = env.str("DATE_FORMAT_IN", default="%m/%d/%Y")
DATE_FORMAT_OUT = env.str("DATE_FORMAT_OUT", default="%-m/%-d/%Y")
DATETIME_FORMAT_OUT = env.str("DATE_FORMAT_OUT", default="%-m/%-d/%Y %H:%M:%S")

RangeValueType = Union[pd.DataFrame, pd.Series]
CellValueType = Union[float, int, str, date]
CellIOValueType = Union[str, float, int]
