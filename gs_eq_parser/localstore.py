"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- I/O operations to handle formatting for save and load operations,
 by storing ranges as CSV tables.
"""
import csv
from io import StringIO
from typing import Union

import pandas as pd

from gs_eq_parser.cellref import CellRef


###########################################################################
# Tables and ranges as csv
###########################################################################
def _convert_tables_to_csv(table: Union[pd.DataFrame, pd.Series, str]) -> str:
    params_csv = {"quoting": csv.QUOTE_NONNUMERIC}
    if isinstance(table, pd.Series):
        params_csv["header"] = False
    return table.astype(str).to_csv(**params_csv)


def _post_process_loaded_vector(raw_df, cell: CellRef) -> pd.Series:
    """Special treatment for stored pd.Series."""
    s = raw_df.set_index(raw_df[0].values.astype(int))[1]
    if cell.is_vector_column:
        s.name = cell.initial_row - 1
    elif cell.is_vector:
        s.name = cell.initial_col - 1
    else:  # it's a value
        s.index = [cell.initial_row - 1]
        s.name = cell.initial_col - 1
    return s


def _load_tables_from_csv(
    key_cell: str, csv_repr: str
) -> Union[pd.DataFrame, pd.Series, str]:
    cell = CellRef.from_string(key_cell)
    is_series = cell.is_vector or not cell.is_range
    params = {
        "quoting": csv.QUOTE_NONNUMERIC,
        "header": 0,
        "index_col": 0,
        "keep_default_na": False,
        "dtype": object,
    }
    if is_series:
        params.update({"header": None, "index_col": None, "squeeze": False})

    table = pd.read_csv(StringIO(csv_repr), **params)
    if is_series:
        return _post_process_loaded_vector(table, cell)

    # Ensure numeric axes
    assert table.index.is_numeric()
    table.columns = list(map(int, table.columns))

    return table


###########################################################################
# STORE / LOAD as JSON
###########################################################################
def make_dict_for_save(raw_data: dict) -> dict:
    """
    Generate a JSON-compatible dict representation of the internal data needed
    to regenerate the extracted spreadsheet logic.
    """
    return {
        "used_sheet_names": list(sorted(raw_data["_sheets"])),
        "config": raw_data["config"].to_dict(),
        "raw_resources": {
            cell.ref: _convert_tables_to_csv(value)
            for cell, value in raw_data["_raw_resources"].items()
        },
    }


def load_from_dict(data: dict) -> dict:
    """
    Decode csv tables inside the stored dict data and pre-process the data
    before recreation of GSheetLogicParser object
    """
    data["sheets"] = data.pop("used_sheet_names")
    data["raw_resources"] = {
        k: _load_tables_from_csv(k, v)
        for k, v in data.pop("raw_resources").items()
    }

    config = data.pop("config")
    data["sheet_name"] = config["sheet_name"]
    data["inputs"] = config["inputs"]
    data["outputs"] = config["outputs"]
    return data
