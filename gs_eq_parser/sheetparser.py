"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- GSheetLogicParser: main object to handle all the operations
"""
import logging
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd

from gs_eq_parser.cellexpression import CellExpressionTree, CellExprType
from gs_eq_parser.cellref import CellRef, output_cell_value, parse_cell_value
from gs_eq_parser.definitions import CellValueType, RangeValueType
from gs_eq_parser.sheetconfig import SheetConfig
from gs_eq_parser.utils import (
    filter_formula,
    get_resource_from_cell,
    is_expression,
    make_cell_ref,
    rebuild_range_data,
    timeit,
)


def _get_cell_coord_from_flat(
    flat_pos: int, cell: CellRef, table: RangeValueType
) -> Tuple[int, int]:
    """Obtain cell coordinates from flat position in range."""
    if cell.is_vector_column:
        coord_cell = table.index[flat_pos], table.name
    elif cell.is_vector or not cell.is_range:
        coord_cell = table.name, table.index[flat_pos]
    else:
        row = flat_pos // table.shape[1]
        col = flat_pos % table.shape[1]
        coord_cell = table.index[row], table.columns[col]
    return coord_cell


def _iterate_over_table(
    cell: CellRef, table: RangeValueType
) -> Tuple[int, Tuple[int, int], CellValueType]:
    """Generator of (flat_pos, coord_cell, value) tuples from cell and table"""
    arr_values = table.values.ravel()
    for flat_pos, raw_v in enumerate(arr_values):
        coord_cell = _get_cell_coord_from_flat(flat_pos, cell, table)
        yield flat_pos, coord_cell, raw_v


def _num_eqs_in_table(table) -> int:
    return sum(map(is_expression, table.values.ravel()))


def _cell_keys_for_formula_cells(
    rg_cell: CellRef, range_values: RangeValueType
) -> Tuple[List[str], List[CellValueType]]:
    """TODO doc"""
    flat_values = range_values.values.ravel()
    flat_positions = np.where(np.array(list(map(is_expression, flat_values))))

    def _make_key_from_pos(x):
        coords = _get_cell_coord_from_flat(x, rg_cell, range_values)
        return make_cell_ref(*coords, sheet_name=rg_cell.remote_sheet)

    keys = list(map(_make_key_from_pos, flat_positions[0]))
    values = flat_values[flat_positions].tolist()

    return keys, values


class GSheetParser:
    """
    Object to handle the different steps for the GSheet logic extraction

    1. Download 'main' worksheet to pd.DataFrame
    2. Download formulae for value column in main worksheet
    3. Download & store all referenced table ranges and values in formulae
    4. Parse expressions inside formulae and solve them obtaining a symbolic
    representation of each one of them, for posterior usage.
    5. Save all data to disk to operate offline without any download / Load it.
    6. Change input parameter values and re-evaluate the formulae.
    7. Report changes in values, variable-value pairs, etc.
    """

    # Internal
    _raw_text_substitutions: Dict[str, str]
    _values: Dict[str, CellValueType]
    _subexpressions: Dict[str, CellExpressionTree]

    # IN fields
    sheets: List[str]
    config: SheetConfig

    # IN/OUT fields
    raw_resources: Dict[CellRef, RangeValueType]

    # Output
    resources: Dict[str, Tuple[CellRef, RangeValueType]]

    def __init__(
        self,
        config: SheetConfig,
        sheets: list,
        raw_resources: Dict[CellRef, RangeValueType],
        do_preprocess: bool = True,
    ):
        self.config = config

        # Containers for expressions and values
        self.sheets = sheets
        self.raw_resources = raw_resources.copy()
        self.resources = {}

        self._subexpressions = {}
        self._raw_text_substitutions = {}
        self._values = {}

        # Parse data
        self.parse_sheets(do_preprocess)

    @staticmethod
    def _reconstruct_rsc(table_expr: RangeValueType) -> RangeValueType:
        """
        Redo the raw resource (text table with equations) after
        the logic simplification, to be stored on disk as simple as possible.
        """

        def _reconstruct(x):
            """Recreate eq from expr. or gen. the output version of value."""
            if is_expression(x):
                return f"={x.text}"
            return output_cell_value(x)

        rec = np.array(list(map(_reconstruct, table_expr.values.ravel())))
        return rebuild_range_data(rec, table_expr)

    @timeit(level=logging.WARNING, minimum_time=0.5)
    def parse_sheets(self, do_preprocess=True):
        """Parse the equations in used sheets."""
        # Parse objective column from main sheet to get formulae to parse
        input_coords = {c.coords: c for c in self.config.input_cells}

        # Parse formulae in raw resources and store parsed tables as resources
        self.resources: Dict[str, Tuple[CellRef, RangeValueType]] = {
            range_cell.remote_sheet: (
                range_cell,
                self._parse_equations_in_df(range_cell, table, input_coords),
            )
            for range_cell, table in self.raw_resources.items()
        }

        if not self.config.use_generic_ranges:
            # Extract inputs and outputs from main sheet by column
            main_rsc = next(
                filter(lambda x: x == self.config.sheet_name, self.resources)
            )
            main_df: pd.DataFrame = self.resources[main_rsc][1]

            # Generate new standard config from alternate configuration
            self.config = SheetConfig.from_column_definition(
                self.config, main_df
            )

        if do_preprocess:
            # Try to simplify and reconstruct the parsed expressions
            forbidden_keys = self.config.forbidden_keys
            forbidden_coords = self.config.forbidden_coords
            for (range_cell, table) in self.resources.values():
                num_eq_remain, table = self._simplify_exprs_in_parsed_df(
                    range_cell, table, forbidden_keys, forbidden_coords,
                )

                # Reconstruct formulae representation for disk storage
                table_rsc = table
                if num_eq_remain:
                    table_rsc = self._reconstruct_rsc(table)
                self.raw_resources[range_cell] = table_rsc

        # Extract inputs and equations with their labels
        self.config.set_labels(self._rsc_getter)

    def _set_value_in_rsc(self, value, sheet_name, cell_coords):
        rsc_cell = self.resources[sheet_name][0]
        if rsc_cell.is_vector_column:
            self.resources[sheet_name][1].loc[cell_coords[0]] = value
        elif rsc_cell.is_vector or not rsc_cell.is_range:
            self.resources[sheet_name][1].loc[cell_coords[1]] = value
        else:
            self.resources[sheet_name][1].loc[cell_coords] = value

    def _recursive_try_eval(
        self,
        sheet_ref: CellRef,
        val: Union[CellExpressionTree, CellValueType],
        excluded: List[str],
    ):
        if not is_expression(val):
            return val

        elif val.ex_type.is_variable:
            if val.variable.ref in excluded:
                # can't do more right now :(
                return val

            # Follow linked variables
            return self._recursive_try_eval(
                sheet_ref, self._rsc_getter(val.variable), excluded,
            )

        elif val.is_time_dependent_expression:
            # Cannot evaluate a time dependent expression in simplification
            return val

        dep_vars = val.dependent_variables + val.dependent_ranges
        if any(v.variable.ref in excluded for v in dep_vars):
            # Depends of input or output cells, not evaluating it
            return val

        # could be a formula _input-independent_, let's try to eval it
        can_be_done = True
        for v in filter(
            lambda x: x.variable.ref not in self._values, dep_vars,
        ):
            range_values = self._rsc_getter(v.variable)
            if v.ex_type == CellExprType.DATA_RANGE:
                n_dep_eq = _num_eqs_in_table(range_values)
                if n_dep_eq == 0:
                    # range of just values :)
                    self._update_value(v.variable.ref, range_values)
                    v.last_eval_value = range_values
                    continue

                # If there are eqs, check dependencies in there
                cell_keys, cell_values = _cell_keys_for_formula_cells(
                    v.variable, range_values
                )
                for _cell_key, cell_value in filter(
                    lambda x: x[0] not in self._values,
                    zip(cell_keys, cell_values),
                ):
                    res_dep = self._recursive_try_eval(
                        sheet_ref, cell_value, excluded,
                    )
                    if res_dep is None or is_expression(res_dep):
                        can_be_done = False
                        break
                    n_dep_eq -= 1

                logging.info(
                    f"Adding new range {v.variable.ref} to values with"
                    f" shapes C:{v.variable.shape}, "
                    f"Data:{range_values.shape}. {n_dep_eq} eqs contained!"
                )
            elif not is_expression(range_values):
                self._update_value(v.variable.ref, range_values)
                v.last_eval_value = range_values
            else:
                res_dep = self._recursive_try_eval(
                    sheet_ref, range_values, excluded,
                )
                if res_dep is None or is_expression(res_dep):
                    can_be_done = False
                    break

                self._set_value_in_rsc(
                    res_dep, v.variable.remote_sheet, v.variable.coords
                )
                self._update_value(v.variable.ref, res_dep)
                v.last_eval_value = res_dep

        if can_be_done:
            return val.evaluate(self._values, self._rsc_getter)

        return val

    def _simplify_exprs_in_parsed_df(
        self,
        cell_df: CellRef,
        table_eqs: RangeValueType,
        forbidden_keys: List[str],
        forbidden_coords: List[Tuple[str, int, int]],
    ) -> Tuple[int, RangeValueType]:
        """
        Simplify resource DataFrame by evaluating permanently the formulae
        which depends only on constant values in the same sheet
        or in cells considered constants.
        """
        sheet_name = cell_df.remote_sheet
        num_eqs_contained = _num_eqs_in_table(table_eqs)
        final_balance = 0
        if num_eqs_contained == 0:
            logging.debug(
                f"{sheet_name} is a referenced sheet containing only values, "
                f"with shape {table_eqs.shape} and reference {cell_df.ref}"
            )
            return final_balance, table_eqs

        for _, coord_cell, value in _iterate_over_table(cell_df, table_eqs):
            if (sheet_name, *coord_cell) in forbidden_coords:
                continue
            elif value in ("", None):
                continue
            elif not is_expression(value):
                continue

            evaluated = self._recursive_try_eval(
                cell_df, value, forbidden_keys
            )

            if evaluated is not None and value != evaluated:
                self._set_value_in_rsc(evaluated, sheet_name, coord_cell)

        # Check results:
        final_balance = _num_eqs_in_table(table_eqs)
        if not final_balance:
            logging.info(
                f"Successful permanent evaluation of sheet {sheet_name}. "
                f"Now using it as just constant data. "
                f"It had {num_eqs_contained} equations inside."
            )
        elif num_eqs_contained == final_balance:
            logging.info(
                f"Bad try to evaluate sheet {sheet_name}, which contains "
                f"{num_eqs_contained} equations. No one can be solved now!"
            )
        else:
            logging.info(
                f"Some ({num_eqs_contained - final_balance}) of "
                f"the expressions contained in {sheet_name} had been "
                f"permanently evaluated to simple values, but "
                f"{final_balance} externally dependent "
                f"equations remain in the sheet."
            )

        return final_balance, table_eqs

    def _parse_equations_in_df(
        self, cell: CellRef, raw_table: RangeValueType, input_coords: dict
    ) -> RangeValueType:
        """
        Analyze (down)loaded tables for some cell range to:
        - parse formulae contained on them,
        - try to evaluate all expressions that are dependant only on the
          same sheet, like equations to generate values that remain constant,
          to be converted to values.
        """
        sheet_name = cell.remote_sheet
        input_coords_local = dict(
            filter(
                lambda x: x[1].remote_sheet == sheet_name, input_coords.items()
            )
        )
        arr_values = raw_table.values.ravel()
        for flat_p, coord_cell, raw_v in _iterate_over_table(cell, raw_table):
            if coord_cell in input_coords_local:
                # parse input cells as variables
                value = parse_cell_value(raw_v)
                c_input = CellExpressionTree(
                    ex_type=CellExprType.VARIABLE,
                    variable=input_coords_local[coord_cell],
                    last_eval_value=value,
                )
                self._subexpressions[c_input.variable.ref] = c_input
                arr_values[flat_p] = value
            elif filter_formula(raw_v) is not None:
                # parse cells with formulae
                expr = CellExpressionTree.parse(
                    eq=raw_v[1:],
                    local_sheet_name=sheet_name,
                    known_subs=self._subexpressions,
                    raw_text_subs=self._raw_text_substitutions,
                    sheet_names=self.sheets,
                )
                key = expr.expr_id(self._raw_text_substitutions)
                self._subexpressions[key] = expr
                arr_values[flat_p] = expr
            else:
                # parse cells with values
                arr_values[flat_p] = parse_cell_value(raw_v)

        return rebuild_range_data(arr_values, raw_table)

    def _update_value(self, key: str, value: CellValueType) -> CellValueType:
        if key not in self._values:
            self._values[key] = value
        return value

    def _rsc_getter(self, cell: CellRef):
        # Get from resources
        extracted = get_resource_from_cell(cell, self.resources)
        if not is_expression(extracted):
            return self._update_value(cell.ref, extracted)
        return extracted
