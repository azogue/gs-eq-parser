"""GSheet formula parser to import, analyze & use spreadsheet logic offline."""
from envparse import env

from gs_eq_parser.cellexpression import CellExpressionTree, CellExprType
from gs_eq_parser.cellref import CellRef, output_cell_value, parse_cell_value
from gs_eq_parser.dataextractor import GSheetLogic
from gs_eq_parser.sheetdownloader import download_sheets_data

# load .env variables (for the credentials to the GSheet API)
env.read_envfile()

__version__ = "0.3.0"
__all__ = [
    "CellExpressionTree",
    "CellExprType",
    "CellRef",
    "download_sheets_data",
    "GSheetLogic",
    "output_cell_value",
    "parse_cell_value",
]
