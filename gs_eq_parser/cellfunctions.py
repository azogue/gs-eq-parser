"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

* Implemented spreadsheet functions (VLOOKUP, IF, ...)

- apply_known_function: Method to apply a function
- KNOWN_FUNCS: Dict to store known Excel-like functions used in formulae.
"""
import logging
import re
from collections import Counter, namedtuple
from datetime import date, datetime
from math import ceil, cos, exp, floor, log, log10, pi, sin, sqrt, tan
from typing import Callable, Union

import pandas as pd

from gs_eq_parser.cellref import (
    output_cell_value,
    parse_cell_value,
    strip_quotes,
)
from gs_eq_parser.definitions import DATE_FORMAT_OUT, DATETIME_FORMAT_OUT
from gs_eq_parser.utils import is_iterable

# KNOWN_FUNCS
KnownFunc = namedtuple(
    "KnownFunc", ["function", "min_args", "max_args", "out_type"]
)
_UNKNOWN_FUNCS = Counter()


# Used regular expressions
RG_VALUE_FMT = re.compile(
    r"(?P<total>(?P<left>[0#]+(?P<thousands>,000|,###)?)"
    r"(?P<sep>\.)?(?P<right>[0#]+)?)"
)
RG_THOUSANDS = re.compile(r"(?P<num1>\d+)(?P<num2>\d{3}(?:.\d{1,12})?)")


def _cell_lookup(
    target: Union[str, float, int],
    rg_search: pd.DataFrame,
    column: int,
    _is_sorted: int = 1,
    is_vertical: bool = True,
):
    """Vertical / Horizontal search (VLOOKUP/HLOOKUP)."""
    exact_match = int(_is_sorted) == 0

    if isinstance(rg_search, pd.Series):
        rg_search = pd.DataFrame(rg_search)

    # use output representation to search
    target = output_cell_value(target, force_str=True)
    if is_vertical:
        found = rg_search[rg_search.iloc[:, 0].astype(str) == target]
    else:
        found = rg_search.T[rg_search.iloc[0, :].astype(str) == target]

    if found.empty and exact_match:
        return None

    elif found.empty:
        found = rg_search.iloc[-1]

    else:
        found = found.iloc[0]

    return parse_cell_value(found.iloc[column - 1])


def _cell_function_vert_lookup(
    target: Union[str, float, int],
    rg_search: pd.DataFrame,
    column: int,
    _is_sorted: int = 1,
):
    """Vertical search (VLOOKUP)."""
    return _cell_lookup(target, rg_search, column, _is_sorted, True)


def _cell_function_horiz_lookup(
    target: Union[str, float, int],
    rg_search: pd.DataFrame,
    row: int,
    _is_sorted: int = 0,
):
    """Horizontal search (HLOOKUP)."""
    return _cell_lookup(target, rg_search, row, _is_sorted, False)


def _cell_function_match(
    target: Union[str, float, int], s_search: pd.Series, _inexact: int = 0
):
    """Get relative position of item in interval."""
    _inexact = int(_inexact)
    new_target = strip_quotes(str(target))
    if _inexact != 0:  # pragma: no cover
        logging.warning(f"Unimplemented MATCH(_inexact={_inexact})")
    possibles = s_search.tolist()
    try:
        return possibles.index(new_target) + 1
    except ValueError:
        return possibles.index(target) + 1


def _cell_function_mid(target, start, length):
    return strip_quotes(str(target))[
        int(start) - 1 : int(start) + int(length) - 1
    ]


def _cell_function_find(text_to_find, text_where_to_look, initial_pos=1):
    text_to_find = strip_quotes(str(text_to_find))
    text_where_to_look = strip_quotes(str(text_where_to_look))
    initial_pos = int(initial_pos) - 1
    return text_where_to_look[initial_pos:].index(text_to_find) + 1


def _cell_function_concatenate(*values):
    return "".join(map(strip_quotes, map(str, values)))


def _cell_function_join(sep: str, *values):
    if len(values) == 1:
        value = values[0]
        if isinstance(value, pd.Series):
            iterator = map(strip_quotes, map(str, value.tolist()))
        else:
            iterator = map(strip_quotes, map(str, list(value)))
    else:
        iterator = map(strip_quotes, map(str, values))
    return strip_quotes(sep).join(iterator)


def _cell_function_split(
    text: str, sep: str, _div_by_char: int = 1, _remove_empty: int = 1
):
    if _div_by_char:
        rg = re.compile("|".join(sep[:]))
        result = rg.split(text)
    else:
        result = text.split(sep)
    if _remove_empty:
        result = list(filter(lambda x: len(x.strip()), result))

    # TODO implement matrix formulae!
    return result


def _cell_function_if(condition, value_true, value_false=0):
    if condition:
        return value_true
    return value_false


def _cell_function_if_error(value_can_be_error, alternate_value=0):
    if not value_can_be_error:
        return alternate_value
    return value_can_be_error


def cell_function_today():
    """Return datetime.date() object to handle operations with dates."""
    return datetime.today().date()


def cell_function_now():
    """Return datetime object to handle operations with dates."""
    return datetime.now()


def _cell_function_value(x):
    return parse_cell_value(x)


def _cell_function_mround(value, factor):
    sign_factor = 1
    if value < 0 and factor < 0:
        value *= -1
        factor *= -1
        sign_factor = -1
    res = round(
        round((value + factor / 1000) / factor) * factor, ceil(-log10(factor))
    )
    return sign_factor * res


def _cell_function_ceiling(value, factor=1):
    return ceil(value / factor) * factor


def _cell_function_floor(value, factor=1):
    return floor(value / factor) * factor


def _apply_agg_function(func: Callable, func_name: str, *values):
    """
    Common method for aggregation functions like sum, min, max.

    It receives a variable number of arguments, and each of them can be:
        * A scalar, so no aggregation is needed. Ex: `=SUM(1, ...)`
        * A vector or a matrix (Series or DataFrame), like `=SUM(A1:C7, ...)`
          and agg in all axes is done to produce a scalar in return
        * An iterable of any other type, and aggregation method is called on it
    """

    def _apply_and_reduce_dimension(value):
        if isinstance(value, pd.DataFrame):
            # Like doing a `value.max().max()`
            return getattr(getattr(value, func_name)(), func_name)()
        elif isinstance(value, pd.Series):
            # Like doing a `value.max()`
            return getattr(value, func_name)()

        elif is_iterable(value):
            return func(value)
        return value

    if len(values) == 1:
        return _apply_and_reduce_dimension(values[0])
    return func(_apply_and_reduce_dimension(arg) for arg in values)


def _cell_function_max(*values):
    return _apply_agg_function(max, "max", *values)


def _cell_function_min(*values):
    return _apply_agg_function(min, "min", *values)


def _cell_function_sum(*values):
    return _apply_agg_function(sum, "sum", *values)


def _cell_function_round(value, factor):
    return round(value, factor)


def _cell_function_text(number, fmt=None):
    if isinstance(number, date):
        is_dt = isinstance(number, datetime)
        if fmt is None:
            fmt = DATETIME_FORMAT_OUT if is_dt else DATE_FORMAT_OUT
        return number.strftime(fmt)
    elif fmt is None:
        return strip_quotes(str(number))

    number = parse_cell_value(number)
    found = RG_VALUE_FMT.findall(fmt)
    if not found:
        logging.warning(
            f"Format not recognized in TEXT() function with value={number} "
            f"and format={fmt}"
        )
        return strip_quotes(str(number))

    total, left, thousands, sep, right = found[0]
    l1 = len(left.replace(",", ""))
    l2 = len(right)
    if not right:
        mode = "g"  # mode = "g" if left[-1] == "#" else "f"
    else:
        mode = "g" if right[-1] == "#" else "f"
    if mode == "g":
        if not l2:
            out_fmt = f"0{l1}{mode}"
        else:
            out_fmt = f".{l1 + l2}{mode}"
    else:
        out_fmt = f"{l1}.{l2}{mode}"

    if fmt.endswith("%"):
        number *= 100

    if "." not in out_fmt and isinstance(number, float):
        number = round(number)

    number_repr = "%{0}".format(out_fmt) % number
    if thousands:
        number_repr = RG_THOUSANDS.sub(r"\1,\2", number_repr)
    return RG_VALUE_FMT.sub(number_repr, fmt, count=1)


# def _cell_function_choose(selection, *options):
#     try:
#         chosen = options[int(selection)]
#         return chosen
#     except ValueError as exc:
#         logging.warning(
#             f"BAD `CHOOSE` between {len(options)} options. "
#             f"Selection is {selection}. ValueError: {exc}"
#         )
#         return options[0]


def _cell_function_sumifs(range_sum: pd.Series, *extra_checks):
    condition = 1
    for range_check, criteria in zip(extra_checks[::2], extra_checks[1::2]):
        sub_condition = range_check == criteria
        condition &= sub_condition
    subset_sum = range_sum[condition]
    return subset_sum.sum()


def _cell_function_dummy_arrformula(matrix_value, *_args):
    """
    ARRAYFORMULA(matrix_value) -> ??
    """
    if matrix_value in ["", '""', None, 0, 0.0]:
        return matrix_value

    logging.warning(f"In ARRAY_FORMULA({matrix_value})")
    return matrix_value


def _cell_function_array_constrain(df_in, num_rows, num_cols):
    """
    ARRAY_CONSTRAIN(range_in, num_rows, num_cols) -> range_out
    """
    if df_in is None:
        # quick exit for null values
        return None

    num_rows = int(num_rows)
    num_cols = int(num_cols)
    if num_cols == 1 and num_rows == 1:
        return df_in

    try:
        return df_in.iloc[:num_rows, :num_cols]
    except AttributeError as exc:
        logging.error(
            f"Bad ARRAY_CONSTRAIN(range, {num_rows}, {num_cols}), "
            f"range:= {df_in}. Exc: {exc}"
        )

        return df_in


# KnownFunc := namedtuple(["function", "min_args", "max_args", "out_type"])
KNOWN_FUNCS = {
    "INT": KnownFunc(int, 1, 1, int),
    "ABS": KnownFunc(abs, 1, 1, None),
    "EXP": KnownFunc(exp, 1, 1, None),
    "LOG": KnownFunc(log, 1, 1, None),
    "SQRT": KnownFunc(sqrt, 1, 1, None),
    "TODAY": KnownFunc(cell_function_today, 0, 0, date),
    "NOW": KnownFunc(cell_function_now, 0, 0, datetime),
    "MID": KnownFunc(_cell_function_mid, 3, 3, str),
    "AND": KnownFunc(lambda *x: all(x), 1, None, bool),
    "OR": KnownFunc(lambda *x: any(x), 1, None, bool),
    "CONCATENATE": KnownFunc(_cell_function_concatenate, 1, None, str),
    "JOIN": KnownFunc(_cell_function_join, 2, None, str),
    "SPLIT": KnownFunc(_cell_function_split, 2, 4, None),
    "IF": KnownFunc(_cell_function_if, 2, 3, None),
    "MATCH": KnownFunc(_cell_function_match, 2, 3, None),
    "VLOOKUP": KnownFunc(_cell_function_vert_lookup, 3, 4, None),
    "HLOOKUP": KnownFunc(_cell_function_horiz_lookup, 3, 4, None),
    "VALUE": KnownFunc(_cell_function_value, 1, 1, None),
    "FIND": KnownFunc(_cell_function_find, 2, 3, None),
    "IFERROR": KnownFunc(_cell_function_if_error, 2, 2, None),
    "MROUND": KnownFunc(_cell_function_mround, 2, 2, None),
    "ROUND": KnownFunc(_cell_function_round, 2, 2, None),
    "SIN": KnownFunc(lambda x: sin(x), 1, 1, float),
    "COS": KnownFunc(lambda x: cos(x), 1, 1, float),
    "TAN": KnownFunc(lambda x: tan(x), 1, 1, float),
    "CEILING": KnownFunc(_cell_function_ceiling, 1, 2, None),
    "FLOOR": KnownFunc(_cell_function_floor, 1, 2, None),
    "MAX": KnownFunc(_cell_function_max, 1, None, None),
    "MIN": KnownFunc(_cell_function_min, 1, None, None),
    "SUM": KnownFunc(_cell_function_sum, 1, None, None),
    "PI": KnownFunc(lambda: pi, 0, 0, float),
    "TEXT": KnownFunc(_cell_function_text, 1, 2, str),
    "ARRAY_CONSTRAIN": KnownFunc(_cell_function_array_constrain, 3, 3, None),
    "SUMIFS": KnownFunc(_cell_function_sumifs, 3, None, None),
    # "CHOOSE": KnownFunc(_cell_function_choose, 2, None, None),
    # TODO as required: implement more excel functions here
    "ARRAYFORMULA": KnownFunc(_cell_function_dummy_arrformula, 1, None, None),
    # "LINEST": KnownFunc(_cell_function_mround, 2, 2, None),
}
TIME_DEPENDENT_FUNCS = ["TODAY", "NOW"]


def has_valid_number_of_args(func_name: str, num_args: int, eq: str) -> bool:
    """Check if a known function has the correct number of parameters."""
    func_name = func_name.upper()
    if func_name not in KNOWN_FUNCS:
        _UNKNOWN_FUNCS[func_name] += 1
        if _UNKNOWN_FUNCS[func_name] == 1:
            logging.warning(
                f"Func {func_name} not recognized when parsing '{eq}'"
            )
        return True

    kf: KnownFunc = KNOWN_FUNCS[func_name]
    if num_args < kf.min_args:
        logging.warning(
            f"Too few args({num_args}) for: {func_name}({kf.min_args}) "
            f"found when parsing '{eq}'"
        )
        return False
    elif (
        kf.max_args is not None and num_args > kf.max_args
    ):  # pragma: no cover
        logging.warning(
            f"Too many args({num_args}) for: {func_name}({kf.max_args}) "
            f"found when parsing '{eq}'"
        )
        return False

    return True


def apply_known_function(kf: KnownFunc, *args):
    """Apply known function, with validation over typing and parameters."""
    if len(args) < kf.min_args:
        logging.warning(
            f"Bad apply function {kf.function.__name__}; "
            f"too few arguments ({len(args)}), minimum is {kf.min_args}"
        )
    if kf.max_args is not None and len(args) > kf.max_args:
        logging.warning(
            f"Bad apply function {kf.function.__name__}; "
            f"too many arguments ({len(args)}), max is {kf.max_args}"
        )  # pragma: no cover

    out = kf.function(*args)
    if kf.out_type is not None and not isinstance(out, kf.out_type):
        logging.warning(
            f"Bad apply function {kf.function.__name__}; "
            f"output type ({type(out).__name__}) "
            f"is not what was expected ({kf.out_type.__name__})"
        )  # pragma: no cover

    return out


if __name__ == "__main__":
    import json

    print(json.dumps(list(sorted(KNOWN_FUNCS.keys())), indent=4))
