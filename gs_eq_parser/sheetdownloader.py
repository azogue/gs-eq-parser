"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- GSheetLogicParser: main object to handle all the operations
"""
import logging
import re
from itertools import chain
from pathlib import Path
from typing import Dict, List, Optional, Set, Tuple

import numpy as np
import pandas as pd
from envparse import env
from gspread import Worksheet

from gs_eq_parser.cellcoords import get_column_letter
from gs_eq_parser.cellref import CellRef, clean_sheet_name, RG_CELL_REFERENCE
from gs_eq_parser.credentials import authorize_access_to_gspread as authorize
from gs_eq_parser.definitions import RangeValueType
from gs_eq_parser.utils import filter_formula, timeit

DEFAULT_SHEET_KEY = env.str("GSHEET_KEY_REMAK_TRANSLATOR", default=None)

# Externally linked sheets, with IMPORTRANGE
RG_GSHEET_URL = re.compile(
    r"https://docs.google.com/spreadsheets/d/(?P<sheet_key>[^/]*)/"
)
RG_IMPORTRANGE = re.compile(
    r"IMPORTRANGE\(\"(?P<ext_sheet>[^\"]*)\",\s?\"(?P<range>[^)\"]*)\"\)"
)
v_import_range_search = np.vectorize(
    lambda x: isinstance(x, str) and bool(RG_IMPORTRANGE.search(x))
)


def _extract_used_sheet_names(
    raw_df: RangeValueType,
) -> Tuple[Set[str], Set[Tuple[str, str]]]:
    """
    Analyze raw equations in downloaded sheet searching for more referenced
    sheets to download.

    This process also looks for imported ranges from other worksheets,
    via "IMPORTRANGE" function, which includes the externally linked sheet url,
    and so the sheet key needed to access it.
    """

    raw_eqs_arr = np.array(list(map(filter_formula, raw_df.values.ravel())))
    if not np.count_nonzero(raw_eqs_arr):
        # table with only values :)
        return set(), set()

    # Import ranges from other worksheets via "IMPORTRANGE" function
    used_external_sheets = set()
    link_positions = v_import_range_search(raw_eqs_arr)
    for row, col in zip(*np.where(link_positions.reshape(raw_df.shape))):
        raw_text = raw_df.loc[row, col]
        for s in RG_IMPORTRANGE.finditer(raw_text):
            found = s.groupdict()
            # External reference can be just the sheet key or a full url
            key = found["ext_sheet"]
            if RG_GSHEET_URL.findall(key):  # pragma: no cover
                key = RG_GSHEET_URL.findall(key)[0]
            c_range = CellRef.from_string(found["range"])
            logging.warning(f"Found external link to {c_range} (key: {key})")

            substitution = c_range.ref
            if c_range.is_range:  # pragma: no cover
                # TODO until ARRAYFORMULA is implemented this would gen errors
                substitution = f"ARRAYFORMULA({c_range.ref})"
                logging.warning(
                    f"External link to {c_range} is a cell range, so it needs "
                    f"to apply ARRAYFORMULA, which is not implemented"
                )
            raw_text = RG_IMPORTRANGE.sub(substitution, raw_text, count=1)
            used_external_sheets.add((key, c_range.remote_sheet))
        raw_df.loc[row, col] = raw_text

    def _extract_ext(x):
        return list(
            map(
                lambda eq: eq[0][:-1].strip("'"),
                RG_CELL_REFERENCE.findall(str(x)),
            )
        )

    used_sheets = set(
        filter(len, set(chain.from_iterable(map(_extract_ext, raw_eqs_arr))))
    )

    return used_sheets, used_external_sheets


def _truncate_initial_empty_space(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remove empty rows and cols in the left/top margins of the table,
    leaving only the content.

    Empty columns or rows INSIDE the content will not be eliminated, so
    lookups indexes are not broken.
    """

    def _truncate(table: pd.DataFrame, columns: bool = True) -> pd.DataFrame:
        idx_use = table.columns if columns else table.index
        sliced_df = table.copy()
        for i, c in enumerate(idx_use):
            vector = table[c] if columns else table.loc[c]
            # check if is an empty vector, so can be removed
            empty = (
                (vector.dtype == np.float)
                and not vector.to_numpy().nonzero()[0].shape[0]
            ) or (
                vector.dtype != np.int64
                and vector.replace({"": None, np.nan: None}).isnull().all()
            )

            if not empty:  # exit (with truncate until this index if any)
                if i > 0:
                    sliced_df = sliced_df.truncate(before=i, axis=int(columns))
                    return sliced_df
                break
        return sliced_df

    simplified = _truncate(df, columns=True)
    simplified = _truncate(simplified, columns=False)
    return simplified.fillna("")


def download_sheet_as_df(sheet: Worksheet) -> Tuple[CellRef, RangeValueType]:
    """
    Download a worksheet from the google sheet as pandas dataframe,
    including formulae as raw strings.

    We download it 2 times, one with just the values and another with the
    formulae. Reason is because the 2nd one misses values in rows that
    contain no formulae (it returns a totally empty row!).
    This way we combine the missing values with the first one,
    so all the info is covered and successfully downloaded.

    Returns the range cell for the downloaded data
    and a dataframe with numeric axes (rows and cols).
    """
    range_text = (
        f"'{sheet.title}'!"
        f"A1:{get_column_letter(sheet.col_count)}{sheet.row_count}"
    )

    df_v = pd.DataFrame(sheet.spreadsheet.values_get(range_text)["values"])
    params_get = {"valueRenderOption": "FORMULA"}
    df_f = pd.DataFrame(
        sheet.spreadsheet.values_get(range_text, params=params_get)["values"]
    )

    def _could_be_hole(x):
        return (isinstance(x, float) and np.isnan(x)) or x in (None, "")

    possible_holes = np.array(list(map(_could_be_hole, df_f.values.ravel())))
    combined = df_f.where(~possible_holes.reshape(df_f.values.shape), df_v)
    raw_table = _truncate_initial_empty_space(combined)

    # Adjust cell range to real size
    range_cell = CellRef.from_string(
        f"{clean_sheet_name(sheet.title)}!"
        f"{get_column_letter(raw_table.columns[0] + 1)}"
        f"{raw_table.index[0] + 1}"
        f":{get_column_letter(raw_table.columns[-1] + 1)}"
        f"{raw_table.index[-1] + 1}"
    )
    if range_cell.is_vector_column:
        raw_table = raw_table.iloc[:, 0]
    elif range_cell.is_vector or not range_cell.is_range:
        raw_table = raw_table.iloc[0, :]
    logging.info(f"Downloaded data range: {range_cell.ref}")
    return range_cell, raw_table


@timeit(level=logging.WARNING)
def download_sheets_data(
    main_sheet_name: str,
    sheet_key=DEFAULT_SHEET_KEY,
    path_credentials: Optional[Path] = None,
) -> Tuple[str, List[str], Dict[CellRef, RangeValueType]]:
    """
    Download all necessary worksheets in the SpreadSheet needed to evaluate the
    defined formulae to solve (as input configuration).
    """
    # Authorize access to spreadsheet and load sheet names
    access = {sheet_key: authorize(sheet_key, path_credentials)}
    sheets: Dict[str, Worksheet] = {
        s.title: s for s in access[sheet_key].worksheets()
    }

    # Initialize containers
    raw_resources: Dict[CellRef, RangeValueType] = {}
    downloaded_sheets = set()
    used_sheets = {main_sheet_name}

    # Collect ranges and cells from all related-by-formulae worksheets
    while not all(x in downloaded_sheets for x in used_sheets):
        used_sheets_in_depth = set()
        for sheet in used_sheets - downloaded_sheets:
            cell_df, raw_df = download_sheet_as_df(sheets[sheet])
            local_sheets, external_sheets = _extract_used_sheet_names(raw_df)
            used_sheets_in_depth.update(local_sheets)
            for (key, ext_sheet) in external_sheets:
                if key not in access:  # New appearance of external worksheet
                    access[key] = authorize(key, path_credentials)
                    new_available_sheets = {
                        s.title: s for s in access[key].worksheets()
                    }
                    if any(k in sheets for k in new_available_sheets):
                        # TODO fix open issue: sheet name collision
                        msg_sheets = ", ".join(
                            [f"SH_{s.title}[k:{k}]" for k, s in access.items()]
                        )
                        logging.error(
                            f"Sheet name *potential* collision problem between"
                            f" linked worksheets {msg_sheets}, when adding"
                            f" {new_available_sheets.keys()} to available"
                            f" collection of {sheets.keys()}"
                        )

                    # Add linked sheets to collection
                    sheets.update(new_available_sheets)
                used_sheets_in_depth.add(ext_sheet)

            raw_resources[cell_df] = raw_df
            downloaded_sheets.add(sheet)

        used_sheets.update(used_sheets_in_depth)

    # Clean main name and return clean used sheets too, with downloaded rsc
    return (
        clean_sheet_name(main_sheet_name),
        list(map(clean_sheet_name, used_sheets)),
        raw_resources,
    )
