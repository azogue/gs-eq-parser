"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

- GSheetLogicParser: main object to handle all the operations
"""
import json
import logging
import pickle
from copy import deepcopy
from functools import partial
from pathlib import Path
from time import time
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
import pandas as pd

from gs_eq_parser.cellexpression import CellExpressionTree, CellExprType
from gs_eq_parser.cellref import CellRef, output_cell_value, parse_cell_value
from gs_eq_parser.definitions import (
    CellIOValueType,
    CellValueType,
    RangeValueType,
)
from gs_eq_parser.localstore import load_from_dict, make_dict_for_save
from gs_eq_parser.sheetconfig import SheetConfig
from gs_eq_parser.sheetdownloader import download_sheets_data
from gs_eq_parser.sheetparser import GSheetParser
from gs_eq_parser.utils import (
    ensure_referenced_cell,
    filter_formula,
    get_resource_from_cell,
    is_expression,
    make_cell_ref,
    timeit,
)


def _output_value(raw_value, force_str=False):
    return output_cell_value(parse_cell_value(raw_value), force_str=force_str)


def _adjust_destination_shape(
    cell_dest: CellRef,
    value_dest: CellValueType,
    expression: Optional[CellExpressionTree],
):
    if len(cell_dest) == 1 and not hasattr(value_dest, "empty"):
        return value_dest
    elif len(cell_dest) == 1 and hasattr(value_dest, "empty"):
        # We have a range, but destiny is a cell
        if (
            expression is not None
            and expression.ex_type == CellExprType.DATA_RANGE
        ):
            # align with range
            row, col = cell_dest.coords
            if isinstance(value_dest, pd.Series):
                if (
                    expression.variable.is_vector_column
                    and row in value_dest.index
                ):  # pragma: no cover
                    return value_dest.loc[row]
                elif col in value_dest.index:
                    return value_dest.loc[col]

        elif type(value_dest) in (pd.Series, pd.DataFrame):  # pragma: no cover
            logging.critical(
                f"Unimplemented feature, {cell_dest} looks like "
                f"a matrix formula!"
            )

    return value_dest


class GSheetLogic:
    """
    Object to handle the different steps for the GSheet logic extraction

    1. Download 'main' worksheet to pd.DataFrame
    2. Download formulae for value column in main worksheet
    3. Download & store all referenced table ranges and values in formulae
    4. Parse expressions inside formulae and solve them obtaining a symbolic
    representation of each one of them, for posterior usage.
    5. Save all data to disk to operate offline without any download / Load it.
    6. Change input parameter values and re-evaluate the formulae.
    7. Report changes in values, variable-value pairs, etc.
    """

    _sheets: List[str]
    _raw_resources: Dict[CellRef, RangeValueType]
    _values: Dict[str, CellValueType]
    _ref_data: Dict[str, CellIOValueType]

    config: SheetConfig
    resources: Dict[str, Tuple[CellRef, RangeValueType]]

    def __init__(
        self,
        sheet_name: str = "DATA",
        inputs: Optional[Dict[str, str]] = None,
        outputs: Optional[Dict[str, str]] = None,
        column_name: Optional[str] = "VARIABLE_NAME",
        column_value: Optional[str] = "VALUE",
        column_category: Optional[str] = None,  # "CATEGORY",
        col_var_name_idx: int = -1,
        col_var_value_idx: int = -1,
        col_var_category_idx: int = -1,
        sheets: list = None,
        raw_resources: Dict[str, pd.DataFrame] = None,
        sheet_key: Optional[str] = None,
        path_credentials: Optional[Path] = None,
        ref_data: Optional[Dict[str, CellIOValueType]] = None,
    ):
        config = SheetConfig.from_user_data(
            sheet_name=sheet_name,
            inputs=inputs,
            outputs=outputs,
            col_name=column_name,
            col_value=column_value,
            col_category=column_category,
            idx_name=col_var_name_idx,
            idx_value=col_var_value_idx,
            idx_category=col_var_category_idx,
        )
        # Containers for expressions and values
        self._sheets = sheets or []
        self._raw_resources = {}
        self._ref_data = ref_data or {}
        self._values = {}
        if raw_resources is not None:
            self._raw_resources = {
                CellRef.from_string(k): v for k, v in raw_resources.items()
            }

        need_preprocess = False
        if sheet_key is not None:  # DOWNLOAD SHEET DATA
            clean_sheetname, used_sheets, raw_resources = download_sheets_data(
                sheet_name, sheet_key, path_credentials
            )
            config.sheet_name = clean_sheetname
            self._sheets = used_sheets
            self._raw_resources = raw_resources
            need_preprocess = True

        if not self._sheets:
            self.config = SheetConfig()
            self.resources = {}
            return

        # Parse data
        parser = GSheetParser(
            config, self._sheets, self._raw_resources, need_preprocess
        )
        self.resources = parser.resources
        self._raw_resources = parser.raw_resources
        self.config = parser.config

    def to_dict(self) -> dict:
        """
        Extract a minimal representation of the expressions and resources used
        in the GSheetLogicParser, to be stored as JSON so the object could
        be loaded from disk, without the need to access the cloud sheet origin.
        """
        return make_dict_for_save(self.__dict__.copy())

    def save(self, path_dest: Union[Path, str], use_pickle=False):
        """Save the GSheetLogicParser to disk as JSON."""
        if use_pickle:  # pragma: no cover
            with open(path_dest, "wb") as f:
                pickle.dump(self, f)
        else:
            params = {"indent": 2, "sort_keys": True, "ensure_ascii": False}
            with open(path_dest, "w") as f:
                json.dump(self.to_dict(), f, **params)

    @classmethod
    def load(cls, path_dest: Union[Path, str], use_pickle=False):
        """Reload the GSheetLogicParser from a stored JSON file."""
        tic = time()
        if use_pickle:  # pragma: no cover
            with open(path_dest, "rb") as f:
                loaded_obj = pickle.load(f)
            logging.info(f"PICKLE file loaded in {time() - tic:.3f} s")
            return loaded_obj

        with open(path_dest) as f:
            raw_loaded = f.read()

        loaded_obj = cls(**load_from_dict(json.loads(raw_loaded)))
        logging.info(f"JSON file loaded in {time() - tic:.3f} s")
        return loaded_obj

    ###########################################################################
    # INPUT DATA
    ###########################################################################
    def _convert_input_labels_to_cell_coords(
        self, input_values: Dict[str, Any]
    ) -> Dict[str, Any]:
        """Check/convert input data to use cell keys."""
        converter: dict = self.config.inputs_dict
        if all(k in converter.keys() for k in input_values.keys()):
            # Already using cell coordinates. Proceed without change
            return input_values

        elif not all(k in converter.values() for k in input_values.keys()):
            not_found = [
                k for k in input_values.keys() if k not in converter.values()
            ]
            msg = (
                f"Some input data keys are not found as variable labels"
                f" or cell coords: {not_found}"
            )
            logging.error(msg)
            raise ValueError(msg)

        # Make conversion from labels to cell coords
        inv_input = {v: k for k, v in self.config.inputs_dict.items()}
        new_input_values = {inv_input[k]: v for k, v in input_values.items()}
        logging.info(
            f"Input keys changed to use cell coordinates, "
            f"from {input_values.keys()} to {new_input_values.keys()}"
        )
        return new_input_values

    def set_input_value(
        self, cell: Union[str, CellRef], new_value: CellIOValueType
    ):
        """Change one input cell value, using input cell reference as key."""
        c = ensure_referenced_cell(cell, self.config.sheet_name)
        if c not in self.config.input_cells:
            raise ValueError(
                f"Cell {c} is not an input cell: {self.config.inputs.values()}"
            )

        rsc_cell, rsc = self.resources[c.remote_sheet]
        old_value = rsc.loc[c.coords]
        if old_value != new_value:
            rsc.loc[c.coords] = new_value
            logging.info(
                f"Input cell {c.ref} changed from {old_value} to {new_value}"
            )
            return True
        return False

    def set_input_data(self, input_values: Dict[str, Any]):
        """
        Define a new set of input values before sheet evaluation.

        Accepts input data using variable labels or cell references.
        """
        # Check/convert input data to use cell coordinates as keys internally
        input_values = self._convert_input_labels_to_cell_coords(input_values)

        # Set input data
        for cell, value in input_values.items():
            self.set_input_value(cell, value)

    def get_input_data(self, use_labels=True) -> Dict[str, Any]:
        """
        Get the current values of the input cells.

        Output can use input labels (default) or input cell keys.
        """
        input_data = {
            c.ref: self.resources[c.remote_sheet][1].loc[c.coords]
            for c in self.config.input_cells
        }
        if use_labels:
            input_data = {
                self.config.inputs_dict[k]: v for k, v in input_data.items()
            }

        return input_data

    ###########################################################################
    # SOLVER
    ###########################################################################
    def get_cell_in_evaluation(self, cell: CellRef) -> CellValueType:
        assert len(cell) == 1
        return get_resource_from_cell(cell, self.resources)

    @staticmethod
    def _update_value(
        cell: CellRef,
        value: CellValueType,
        expression: Optional[CellExpressionTree] = None,
    ) -> CellValueType:
        fixed_value = _adjust_destination_shape(cell, value, expression)
        if (
            expression is not None
            and expression.last_eval_value != fixed_value
        ):
            expression.last_eval_value = fixed_value

        return fixed_value

    def _extract_from_resources(self, cell: CellRef):
        extracted = get_resource_from_cell(cell, self.resources)

        if is_expression(extracted):
            res2 = extracted.evaluate(
                self._values, getter_known_value=self._extract_from_resources
            )
            return self._update_value(cell, res2, extracted)

        if hasattr(extracted, "empty") and extracted.empty:  # pragma: no cover
            logging.critical(f"Found empty DataFrame for cell: {cell.ref}")
            return extracted

        return self._update_value(cell, extracted)

    @timeit(level=logging.WARNING, minimum_time=0.1)
    def solve(self, use_labels=True) -> Dict[str, CellIOValueType]:
        """
        Call to evaluate all formulae from the value column of main sheet.

        Returns a dict with the results for the output cells,
        using output labels as keys (or using cell keys with use_labels=False).
        """
        # Reset known values:
        self._values = {}
        # Backup original resources before evaluation,
        # as some of the cells may change in this process
        backup_resources = deepcopy(self.resources)

        for cell in self.config.input_cells:
            input_value = self.get_cell_in_evaluation(cell)
            assert not is_expression(input_value)
            self._values[cell.ref] = self._update_value(cell, input_value)

        # Loop over output cells, evaluating formulae
        output_names = self.config.equations_dict

        for cell in sorted(self.config.output_cells):
            key_main_value = cell.ref
            variable_name = output_names[key_main_value]
            expr = self.get_cell_in_evaluation(cell)
            if isinstance(expr, CellExpressionTree):
                value = expr.evaluate(
                    self._values,
                    getter_known_value=self._extract_from_resources,
                )
            else:
                value = expr
                expr = None
                logging.info(
                    f"Output cell {cell.ref}:='{variable_name}' "
                    f"is a constant with value '{value}'"
                )
            self._values[cell.ref] = self._update_value(cell, value, expr)

            if value is None:
                logging.info(
                    f"Bad evaluation of variable {variable_name} "
                    f"in cell {key_main_value} := None"
                )

        # Restore original resources
        self.resources = backup_resources

        # Output results collection as {"variable1": value, ...}
        return self.variables_as_dict(use_labels=use_labels)

    ###########################################################################
    # OUTPUT
    ###########################################################################
    def set_reference_data(self, data: Dict[str, CellIOValueType]):
        self._ref_data = {
            k: _output_value(v, force_str=True) for k, v in data.items()
        }

    def _get_final_value(self, cell: CellRef):
        dest = self.get_cell_in_evaluation(cell)
        _attr = "last_eval_value"
        value = getattr(dest, _attr) if hasattr(dest, _attr) else dest
        return _output_value(value)

    def variables_as_dict(self, use_labels=True) -> Dict[str, CellIOValueType]:
        """Generate a dictionary with the variable - calculated value pairs."""

        def _relax_types(data: dict):
            """
            Downcast to python types for standard, JSON serializable output.
            """
            for k, v in data.items():
                if isinstance(v, pd.np.int64):
                    data[k] = int(v)
                elif isinstance(v, pd.np.float64):
                    data[k] = float(v)

        if use_labels:
            names = self.config.output_labels
        else:
            names = self.config.equation_keys
        eval_expr = list(map(self._get_final_value, self.config.output_cells))
        dict_data = dict(list(zip(names, eval_expr)))
        _relax_types(dict_data)
        return dict_data

    def report(
        self, include_input=True, add_expressions=True, add_formulae=False
    ) -> pd.DataFrame:
        """
        Generate a dataframe with the results of the evaluation, listing
        inputs and outputs and their new values.
        """

        def _make_df_report(
            key_name_pairs: Dict[str, str], cells: List[CellRef]
        ) -> pd.DataFrame:
            df = pd.DataFrame(key_name_pairs.items(), columns=["cell", "name"])
            df["value"] = [self._get_final_value(x) for x in cells]
            df["coord"] = [(x.remote_sheet, *x.coords) for x in cells]
            return df

        df_rep = _make_df_report(
            self.config.equations_dict, self.config.output_cells
        )
        if add_expressions:
            df_rep["expr"] = [
                self.get_cell_in_evaluation(x)
                for x in self.config.output_cells
            ]
        if add_expressions and add_formulae:
            df_rep["formula"] = df_rep["expr"].apply(
                lambda x: x.text if is_expression(x) else x
            )

        if not include_input:
            return (
                df_rep.sort_values(by=["coord"])
                .drop("coord", axis=1)
                .set_index("cell")
            )

        df_rep_in = _make_df_report(
            self.config.inputs_dict, self.config.input_cells
        )
        df_rep_in["is_input"] = True
        df_rep["is_input"] = False
        return (
            pd.concat([df_rep_in, df_rep], axis=0, sort=False)
            .sort_values(by=["coord"])
            .drop("coord", axis=1)
            .set_index("cell")
            .fillna("")
        )

    def compare_results(
        self, verbose=True, reset=False
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Generate comparison between obtained results
        and the ones stored as reference.
        """
        report = self.report(include_input=False)
        if report.empty:
            raise ValueError("Nothing to report :(")
        elif not self._ref_data:
            raise ValueError("Nothing to compare with :(")

        # Compare as strings
        report["value"] = report["value"].apply(
            partial(_output_value, force_str=True)
        )
        report["ref"] = (
            pd.Series(report.index).apply(lambda x: self._ref_data[x]).values
        )
        compare_err = report[report.ref != report.value]

        if verbose and not compare_err.empty:
            num_ch = compare_err.shape[0]
            logging.info(
                f"* Sheet evaluation has produced {num_ch} changes "
                f"in these variables:\n{compare_err}\n"
            )

        logging.info(
            f"· Analyzed {len(self.config.output_cells)} variables, "
            f"over {len(self._sheets)} worksheets, "
            f"with {len(self.config.input_cells)} cells as input data\n"
        )
        if reset:
            # Set the calculated values as the current status of the main sheet
            self.set_reference_data(report.value.to_dict())

        return report, compare_err

    def search_function_usage(self, f_search: str = "ARRAY_CONSTRAIN") -> dict:
        """
        Search appearences of some function in the equations for all sheets,
        returning coordinates of matched cells.
        """

        def _has_function(x: str, func: str) -> bool:
            return filter_formula(x) is not None and func.upper() in x.upper()

        func_search = partial(_has_function, func=f_search)
        usages_found = {}
        for range_cell, df in self._raw_resources.items():
            positions = np.array(list(map(func_search, df.values.ravel())))
            if not np.count_nonzero(positions):
                continue

            coords = list(
                map(
                    lambda c: make_cell_ref(*c),
                    zip(*np.where(positions.reshape(df.shape))),
                )
            )
            usages_found[range_cell.ref] = coords

        return usages_found
