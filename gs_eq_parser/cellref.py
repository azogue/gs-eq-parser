"""
GSheet formula parser to import, analyze & use spreadsheet logic offline

* Cell and cell range syntax and value parsing
- CellRef: attr.s dataclass with the coordinates of some cell or range of cells
- parse_cell_value: Method to parse the internal representation for some value
- output_cell_value: Method to generate an output representation for some value
"""
import logging
import re
from datetime import date, datetime
from difflib import get_close_matches
from math import isnan
from typing import Iterable, List, Optional, Tuple, Union

import attr
import numpy as np
import pandas as pd
from slugify import slugify

from gs_eq_parser.cellcoords import get_column_letter, range_boundaries
from gs_eq_parser.definitions import (
    CellIOValueType,
    CellValueType,
    DATE_FORMAT_IN,
    DATE_FORMAT_OUT,
    DATETIME_FORMAT_OUT,
    RangeValueType,
)

SPECIAL_CELLTAG = "SPECIAL"
SPECIAL_CELLTAG_STR = "STRING"
regex_cell_reference = (
    r"(?:(?P<remote>[^'<>=^\-+/*!:,;\s()\"&]+!|'[^'!]+'!)|^|[^_A-Za-z0-9$]*)"
    r"(?:(?P<range>\$?[A-Z]{1,3}\$?\d{1,4}"
    r"(?::\$?[A-Z]{1,3}\$?\d{1,4})?)|"
    r"(?P<range_cols>\$?[A-Z]{1,3}\$?:\$?[A-Z]{1,3}\$?)|"
    r"(?P<range_rows>\$?\d{1,4}:\$?\d{1,4}))"
    r"(?:$|[^_(A-Za-z0-9\"'!])"
)
RG_CELL_REFERENCE = re.compile(regex_cell_reference)
_RG_CELL_FULL_REFERENCE = re.compile(f"^{regex_cell_reference}$")
RG_TABLE_REF = re.compile(r"(?:^|\(|,)(?P<table>TAB_\w+|\w+_tab)(?:|\)|,)")
RG_STRING_NOT_NUMBER = re.compile(r"^(?:0[^,.]+)")
RG_FLOAT_NUMBER_WITH_COMMA = re.compile(r"^([+-]?[0-9]+),([0-9]+)$")
COL_MAX_DEFAULT = ROW_MAX_DEFAULT = 1000


NULL_VALUE = "#N/A"
SPECIAL_VALUES = {"TRUE": 1, "FALSE": 0, NULL_VALUE: None}


def clean_sheet_name(sheet_name):
    """
    Returns the slugifyied version of the sheet raw name,
    to use it internally, avoiding messing up with the formula parsing engine.
    """
    return slugify(sheet_name, lowercase=False, separator="_")


def _is_encapsulated_string(value: str) -> bool:
    """Check string for external quotes."""
    return len(value) > 1 and (
        (value[0] == "'" and value[-1] == "'")
        or (value[0] == '"' and value[-1] == '"')
    )


def strip_quotes(value: CellValueType) -> CellValueType:
    """Clean a value from its external quotes, if it's a string with quotes."""
    if isinstance(value, str) and _is_encapsulated_string(value):
        return value[1:-1]
    return value


def parse_cell_value(val: Optional[CellValueType]) -> Optional[CellValueType]:
    """
    Manage special parsing / type casting, mimicking the excel/gsheet behaviour

    * Default type for anything is str, but numeric casting is done to try to
      promote the value type: int > float > date
    * It parses special values as #N/A, TRUE, FALSE
    * Removes unnecessary quotes (internally) from strings
    * Recognizes strings that could promote to number but shouldn't, like "04"
    """
    if val is None:
        return None

    v_type = type(val)
    if v_type in (int, float, np.int_, np.float_, date, datetime):
        return val

    elif val in SPECIAL_VALUES:
        return SPECIAL_VALUES[val]

    if _is_encapsulated_string(val) or RG_STRING_NOT_NUMBER.findall(val):
        return val

    try:
        return int(val)
    except ValueError:
        try:
            if "," in val:
                return float(RG_FLOAT_NUMBER_WITH_COMMA.sub(r"\1.\2", val))
            elif "/" in val:
                return datetime.strptime(val, DATE_FORMAT_IN).date()
            return float(val)
        except (ValueError, TypeError):
            pass
    return val


def output_cell_value(
    value: CellValueType, force_str: bool = False
) -> CellIOValueType:
    """
    Generate output values from internal value types,
    mimicking the excel/gsheet behaviour.

    An _all-as-string_ output mode is also implemented (force_str), used for:
    - Lookup operations, which are done with both target and cell range as text
    - Setting reference results to compare changes in reports
    - Equality comparisons (== / !=), done also with string representation
    - `&` operations, solved as string concatenations.
    """
    if isinstance(value, datetime):
        return value.strftime(DATETIME_FORMAT_OUT)

    elif isinstance(value, date):
        return value.strftime(DATE_FORMAT_OUT)

    elif not isinstance(value, str):
        if value is None or (isinstance(value, float) and isnan(value)):
            return NULL_VALUE

        if isinstance(value, float) and force_str:
            return f"{value+0:g}"

        elif force_str:
            return str(value)

        elif isinstance(value, float) and abs(value - int(value)) < 1e-9:
            return int(value)

        # noinspection PyTypeChecker
        return value

    return strip_quotes(value)


@attr.s(auto_attribs=True, repr=False, eq=False, order=False, hash=False)
class CellRef:
    """
    Cell Reference dataclass, to store the absolute coordinates of some unique
    cell or range of cells, with its associated value.
    """

    # Cell coordinates
    is_referenced: bool = attr.ib(default=False)
    initial_col: int = attr.ib(factory=int)
    initial_row: Optional[int] = attr.ib(default=None)
    final_col: int = attr.ib(factory=int)
    final_row: Optional[int] = attr.ib(default=None)
    remote_sheet: Optional[str] = attr.ib(default=None)
    # Cell value
    value: Optional[CellValueType] = attr.ib(default=None)
    _ref: Optional[str] = attr.ib(default=None)

    def __attrs_post_init__(self):
        """Check integrity of referenced cells."""
        if not self.is_referenced:
            if not isinstance(self.value, str):
                self._ref = str(self.value)
            return

        # Set clean sheet name
        self.remote_sheet = clean_sheet_name(self.remote_sheet)

    @classmethod
    def from_string(
        cls,
        text: str,
        local_sheet_name: str = "DATA",
        sheet_names: Optional[Iterable[str]] = None,
    ):
        """
        Parse some string into a cell,
        which can contain a value,
        or the coordinates for some cell or range of cells.
        """

        def _validate_sheet_name(name, names: Optional[Iterable[str]] = None):
            if names is None or name == SPECIAL_CELLTAG:
                return name
            if name not in names:
                try:
                    match = get_close_matches(name, names)[0]
                    logging.warning(f"Match: {match} from {name}")
                    return match
                except IndexError:
                    raise KeyError(
                        f"ERROR: Can't validate sheet name: {name} "
                        f"with valid names: {sheet_names}"
                    )
            return name

        found_table_data = RG_TABLE_REF.search(text)
        if found_table_data is not None:
            extracted = found_table_data.groupdict()
            if extracted["table"].startswith("TAB_"):
                remote_sheet = extracted["table"][4:]
            else:
                remote_sheet = extracted["table"][:-4].upper()
            return cls(
                True,  # is_referenced
                1,
                1,
                COL_MAX_DEFAULT,
                ROW_MAX_DEFAULT,
                remote_sheet=_validate_sheet_name(remote_sheet, sheet_names),
            )

        found_cell_data = _RG_CELL_FULL_REFERENCE.search(text)
        if found_cell_data is not None:
            extracted = found_cell_data.groupdict()
            remote_sheet = local_sheet_name
            if extracted["remote"]:
                remote_sheet = extracted["remote"][:-1].strip("'")
            if extracted["range"]:
                boundaries = range_boundaries(extracted["range"])
            elif extracted["range_rows"]:
                boundaries = range_boundaries(extracted["range_rows"])
            else:
                boundaries = range_boundaries(extracted["range_cols"])
            return cls(
                True,  # is_referenced
                *boundaries,  # min_c, min_r, max_c, max_r,
                remote_sheet=_validate_sheet_name(remote_sheet, sheet_names),
            )

        # Is a simple value
        return cls(is_referenced=False, value=parse_cell_value(text))

    @property
    def is_range(self) -> bool:
        """Check if cell points to a range of cells (more than one)."""
        return self.is_referenced and (
            (self.initial_col is None and self.final_col is None)
            or self.final_col > self.initial_col
            or (self.initial_row is None and self.final_row is None)
            or self.final_row > self.initial_row
        )

    @property
    def is_vector(self) -> bool:
        """Check if cell points to a vector (row or column) of cells."""
        return self.is_range and (
            (self.final_col == self.initial_col and self.final_col is not None)
            or (
                self.final_row == self.initial_row
                and self.final_row is not None
            )
        )

    @property
    def is_vector_column(self) -> bool:
        """Check if cell points to a column vector (like A1:N1) of cells."""
        return (
            self.is_range
            and self.final_col == self.initial_col
            and self.initial_col is not None
        )

    @property
    def is_date_value(self) -> bool:
        """Check if cell is a date value."""
        return not self.is_referenced and isinstance(self.value, date)

    @property
    def is_str_value(self) -> bool:
        """Check if cell is a string value."""
        return not self.is_referenced and isinstance(self.value, str)

    @property
    def shape(self) -> Tuple[int, int]:
        """Return shape of cell as (num rows, num columns)."""
        if self.is_range:
            if self.final_row is not None and self.initial_row is not None:
                shape_rows = self.final_row - self.initial_row + 1
            else:
                shape_rows = -1  # No range, all rows from some column

            if self.final_col is not None and self.initial_col is not None:
                shape_cols = self.final_col - self.initial_col + 1
            else:
                shape_cols = -1  # No range, all columns from some row
                # can't be both
                assert shape_cols != shape_rows

            return shape_rows, shape_cols
        return 1, 1

    def __len__(self):
        sh = self.shape
        if sh[0] == -1:
            return ROW_MAX_DEFAULT * sh[1]
        elif sh[1] == -1:
            return sh[0] * COL_MAX_DEFAULT
        return sh[0] * sh[1]

    def __hash__(self):
        return hash(self.ref)

    @property
    def simple_ref(self) -> str:
        """For referenced cells, generate local coordinates like `B1:C2`."""
        assert self.is_referenced
        if self.is_range and self.initial_col is None:  # row cell
            return f"{self.initial_row}:{self.final_row}"

        start_letter = get_column_letter(self.initial_col)
        if self.is_range:
            end_letter = get_column_letter(self.final_col)
            if self.final_row is not None and self.initial_row is not None:
                return (
                    f"{start_letter}{self.initial_row}"
                    f":{end_letter}{self.final_row}"
                )
            else:
                return f"{start_letter}:{end_letter}"
        return f"{start_letter}{self.initial_row}"

    @property
    def ref(self) -> str:
        """
        Returns the cell reference as a key, representing the absolute coords
        of the referenced cell: `sheet_name!B1:C2`

        For cells that are values, .ref is None.
        """
        if self._ref is None and self.is_referenced:
            self._ref = f"{self.remote_sheet}!{self.simple_ref}"
        return self._ref

    @property
    def coords(self) -> Union[Tuple[int, int], Tuple[int, int, int, int]]:
        """Return internal coord labels (from Excel-like ones)."""
        assert self.is_referenced
        if self.is_range:
            return (
                self.initial_row - 1 if self.initial_row else None,
                self.initial_col - 1 if self.initial_col else None,
                self.final_row - 1 if self.final_row else None,
                self.final_col - 1 if self.final_col else None,
            )
        return self.initial_row - 1, self.initial_col - 1

    def __contains__(self, item: "CellRef") -> bool:
        """
        Range inclusion.

        ```
        c2 = EqCellRef.from_string('CNTF!$B$3:$H$3')
        c3 = EqCellRef.from_string('CNTF!$B$3:$H$30')
        assert c2 in c3
        ```
        """
        if not self.is_referenced or not item.is_referenced:
            return False
        elif self.remote_sheet != item.remote_sheet:
            return False
        elif (
            (
                item.initial_col is not None
                and item.initial_col < self.initial_col
            )
            or (item.final_col is not None and item.final_col > self.final_col)
            or (
                item.initial_row is not None
                and item.initial_row < self.initial_row
            )
            or (item.final_row is not None and item.final_row > self.final_row)
        ):
            return False

        return True

    def __repr__(self):
        if not self.is_referenced:
            if self.is_str_value:
                return f"<{self.value} [{type(self.value).__name__}]>"
            return f"<{self.ref} [{type(self.value).__name__}]>"

        if self.is_vector:
            str_r = "<Vc "
        elif self.is_range:
            str_r = "<Rg "
        else:
            str_r = "<Cell "
        str_r += self.ref
        if self.is_range:
            str_r += f", S:{self.shape}"
        return str_r + ">"

    def __gt__(self, other: "CellRef") -> bool:
        """
        Order Cells with these criteria:
        * Range cell < simple cell < string value < numeric value < datetime
        """
        if self.is_referenced and other.is_referenced:
            if self.remote_sheet == other.remote_sheet:
                if (
                    self.initial_col is not None
                    and other.initial_col is not None
                ):
                    if self.initial_col > other.initial_col:
                        return True
                    elif self.initial_col < other.initial_col:
                        return False

                if (
                    self.initial_row is not None
                    and other.initial_row is not None
                ):
                    return self.initial_row > other.initial_row

            return self.remote_sheet > other.remote_sheet
        elif self.is_referenced:
            return False
        elif other.is_referenced:
            return True

        if self.is_str_value and not other.is_str_value:
            return False
        elif not self.is_str_value and other.is_str_value:
            return True
        elif self.is_date_value and not other.is_date_value:
            return False
        elif not self.is_date_value and other.is_date_value:
            return True

        return self.value > other.value

    def __eq__(self, other: "CellRef") -> bool:
        return (
            type(other) == type(self)
            and self.is_referenced == other.is_referenced
            and (
                (self.is_referenced and self.ref == other.ref)
                or (not self.is_referenced and self.value == other.value)
            )
        )

    def extract_from_range(
        self, other: "CellRef", range_values: RangeValueType
    ) -> Union[CellValueType, RangeValueType]:
        """
        Extract a data subset from some origin using cell coordinates.

        It needs a `range_values` pandas object which reflects the cells
        contained in the CellRef object, and a second CellRef to indicate
        what to slice.

        If the slicing-cell is not contained in the range, empty strings
        are returned, to mimic the Excel-like behaviour.
        """
        if len(other) == 1:
            # cell extraction
            if not self.is_range:  # 1 to 1 extraction
                return range_values.iloc[0]

            row, col = other.coords
            if isinstance(range_values, pd.Series):
                coord_use = row if self.is_vector_column else col
                if coord_use in range_values.index:
                    return range_values.loc[coord_use]
            else:
                if row in range_values.index and col in range_values.columns:
                    return range_values.loc[other.coords]

            logging.debug(
                f"Can't find {other.ref} in {self.ref} "
                f"(coords: {other.coords}), filling with ''"
            )
            return ""

        elif other.is_vector and not self.is_vector:
            # vector extraction from matrix
            r_start, c_start, r_end, c_end = other.coords
            if other.is_vector_column:
                out = range_values.loc[r_start:r_end, c_start]
            else:
                out = range_values.loc[r_start, c_start:c_end]

        elif other.is_vector and self.is_vector:
            # subset of vector extraction
            assert other.is_vector_column == self.is_vector_column
            r_start, c_start, r_end, c_end = other.coords
            if other.is_vector_column:
                out = range_values.loc[r_start:r_end]
            else:
                out = range_values.loc[c_start:c_end]

        else:
            # sub-matrix from matrix extraction
            r_start, c_start, r_end, c_end = other.coords
            if c_start is None:  # use all columns from resource
                _, c_start, _, c_end = self.coords
            elif r_start is None:  # use all rows from resource
                r_start, _, r_end, _ = self.coords

            if isinstance(range_values, np.ndarray):
                out = range_values[r_start:r_end][c_start:c_end]
            else:
                out = range_values.loc[r_start:r_end, c_start:c_end]

        if isinstance(out, pd.Series):
            return out

        if out.shape == (1, 1):
            return out.iloc[0, 0]  # single cell from pd.DataFrame

        return out


def expand_cell_vector_into_cells(cell_range: CellRef) -> List[CellRef]:
    """
    Create a list of cell references pointing to
    each of the cells contained in given cell range.

    Used to expand user config for input and output cell ranges into individual
    input and output cells.
    """

    if not cell_range.is_range:
        return [cell_range]

    def _params(row_or_col: int, cell_rg: CellRef, is_column: bool) -> dict:
        params = {"is_referenced": True, "remote_sheet": cell_rg.remote_sheet}
        if is_column:
            params.update(
                initial_row=row_or_col,
                final_row=row_or_col,
                initial_col=cell_rg.initial_col,
                final_col=cell_rg.final_col,
            )
        else:
            params.update(
                initial_row=cell_rg.initial_row,
                final_row=cell_rg.final_row,
                initial_col=row_or_col,
                final_col=row_or_col,
            )
        return params

    if cell_range.is_vector_column:
        interval = cell_range.initial_row, cell_range.final_row + 1
    else:
        interval = cell_range.initial_col, cell_range.final_col + 1

    return [
        CellRef(**_params(row, cell_range, True)) for row in range(*interval)
    ]
