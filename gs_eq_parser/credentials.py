"""Authorize access to the Google Sheet API through `gspread`."""
import json
import pathlib

import gspread
from envparse import env
from oauth2client.service_account import ServiceAccountCredentials


def _oauth_credentials_from_env() -> dict:
    # fmt: off
    return {
        "type": "service_account",
        "project_id": "ahu-platform",
        "private_key_id": env.str("GSHEET_PRIVATE_KEY_ID"),
        "private_key": env.str("GSHEET_PRIVATE_KEY"),
        "client_id": env.str("GSHEET_CLIENT_ID"),
        "client_email": env.str("GSHEET_CLIENT_EMAIL"),
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",  # noqa
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/admin-978%40ahu-platform.iam.gserviceaccount.com",  # noqa
    }
    # fmt: on


def _oauth_credentials_from_json(path: pathlib.Path) -> dict:
    with open(str(path)) as f:
        return json.load(f)


def authorize_access_to_gspread(
    spreadsheet_key: str, path_credentials: pathlib.Path = None
) -> gspread.Spreadsheet:
    """
    Authorize access to the given spreadsheet
    :param spreadsheet_key: A key of the spreadsheet.
        Might be found after /d of the spreadsheet URL
        For example if the URL is: https://docs.google.com/
            spreadsheets/d/2l-zdfdfNCBWh-Cu1-m-uaybz2aVVqtfJM6cHGGd[QgSS79sw/
        then key = "2l-zdfdfNCBWh-Cu1-m-uaybz2aVVqtfJM6cHGGd[QgSS79sw"
    :param path_credentials: Optional path to JSON file with alternative creds.
    :return: an instance of Spreadsheet
    """
    scope = ["https://spreadsheets.google.com/feeds"]
    if path_credentials is not None:
        data_credentials = _oauth_credentials_from_json(path_credentials)
    else:
        data_credentials = _oauth_credentials_from_env()

    # noinspection PyProtectedMember,PyPep8
    credentials = ServiceAccountCredentials._from_parsed_json_keyfile(
        data_credentials, scope
    )
    gc = gspread.authorize(credentials)
    return gc.open_by_key(spreadsheet_key)
