# gs-eq-parser

### Description

Google Sheet _formula parser_ to **import the equations and used data resources
from some spreadsheet to operate with it offline** as a simple python object.

After the initial download, no more Google API requests are required, as the
contained logic and resources can be stored on disk and loaded when needed to use.

This library pursues a simple but very common** goal, which is to quickly
mirror some logic defined in 'Excel format' into python code, without
the need to write any Python code, and reflecting changes in the Excel
by just re-download the spreadsheet :)

** For all of those _Excel-lovers_ engineers that have the most complicated
logic defined inside Excel formulas but are in the process of carefully migrate
to a better and more pythonic world :), or just want to enjoy the better
of both without the inconvenience of the jumping in between.

The reason it doesn't use the reference library to work with Excel-like
formatting ([openpyxl](https://bitbucket.org/openpyxl/openpyxl/src/default/))
is because **`openpyxl`** doesn't aim to **_evaluate_** the formulae, which is
the main target for this library.

If the library gets used and needs to increase its capabilities, most probably
`openpyxl` will be required, and move the actual regular expression analysis
to another one based in their
[`Tokenizer`](https://bitbucket.org/openpyxl/openpyxl/src/default/openpyxl/formula/tokenizer.py),
and maybe try to contribute there.

### How it works

The initial implementation is very simple, as it ask for a Spreadsheet
that has a **'main worksheet'** where there are:

* a column with 'variable names'
* a column with 'variable values', which contain formulae to evaluate these cells.

So the objective is to _mirror the logic_ to evaluate all of these
variable name-value pairs (one per row in the main sheet).

The formulae in the variables values column can point internally to any other
cell or range, contained in any worksheet included in the spreadsheet.

Each formula, or expression, is parsed as a _tree-like_ object, and maintained
that way, so every time an expression is evaluated, a complete review of the
obtained result is available, by _rendering_ the tree expression:

```python
from gs_eq_parser.cellexpression import CellExpressionTree

expr = CellExpressionTree.parse(
    '=HLOOKUP($E$10,OTHER!$B$5:$FC$28,MATCH(B34,OTHER!$B$6:$B$28,0)+1,0)',
    local_sheet_name="MAIN",
)
expr.render()
```
```text
# eq:= '=HLOOKUP($E$10,OTHER!$B$5:$FC$28,MATCH(B34,OTHER!$B$6:$B$28,0)+1,0)'

Expr[HLOOKUP(FUNC)] ==> 320
|-- Expr[VARIABLE] -> 'MAIN!E10'; V=<Cell [MAIN]:E10, raw:'MAIN!E10'> ==> RH31C
|-- Expr[DATA_RANGE] -> 'OTHER!$B$5:$FC$28'; V=<Rg [OTHER]:B5:FC28, S:(24, 158), raw:'OTHER!$B$5:$FC$28'>
|-- Expr[ADDITION] ==> 2
|   |-- Expr[MATCH(FUNC)] ==> 1
|   |   |-- Expr[VARIABLE] -> 'B34'; V=<Cell [MAIN]:B34, raw:'B34'> ==> fan_Dsa
|   |   |-- Expr[DATA_RANGE] -> 'OTHER!$B$6:$B$28'; V=<Vc [OTHER]:B6:B28, S:(23, 1), raw:'OTHER!$B$6:$B$28'>
|   |   +-- Expr[VALUE] -> '0'; V=<0 [int]>
|   +-- Expr[VALUE] -> '1'; V=<1 [int]>
+-- Expr[VALUE] -> '0'; V=<0 [int]>
```

** When the expression has been evaluated, the `expr.render()` method shows the
value for each intermediate result in the formula (` ==> xxx` terminations),
so _Excel debugging_ is neat too :)

Also, the solver is sufficiently quick, as it maintains a _cache_ of each
evaluated subexpression, so multiples usages of function calls as
`VLOOKUP(...)` (in the same formula or in other equations) are evaluated just
once when the `.solve()` method is invoked.

### What does the `GSheetLogic` object does internally?

It has 2 ways for creation: (1)download / (2)load from disk;
and a simple way of evaluation: (3)solve

(1) Creation with a sheet_key -> .download(), set preprocess on

- Authorize access to GSheet
- Download main sheet, as values, but also as formulae for the 'VALUE' column
- Extract sheets used in main sheet eqs found in that column
- Download each used sheet as raw. If 'recursive' mode is on, download them
  with formulae, extracting cells and sheets used. While new sheets appear,
  continue downloading them.
- Enable pre-process mode to try to simplify downloaded sheets as possible.

--> Jump to (1) & (2)

(2) Load locally stored JSON representation of GSheetLogic

- Load can force pre-process mode on, but it's off by default as the locally
  stored logic is suposed to be already simplified.

(1) & (2) Prepare loaded/downloaded data to use it, by parsing every formula
present in the data: (`._process_formulae(with_preprocess)`)

- Parse equations of main sheet (only in 'VALUE' column), considering that any
  value in that column which is not a formula, it's an **input** for the logic.
  Every other cell in the main sheet is considered a 'constant'.
- For each resource sheet, identify cells and parse equations inside it.
  If pre-process is on, eqs in resource dataframes are tried to be solved
  with just local data, to simplify lots of equations which are present only
  to generate constant data. After that, resources are regenerated with
  less/simpler expressions (with `expr.text`), so when saved to disk,
  the minimal representation will be stored, and this step wont be needed anymore
  After all resources are processed, another last try to solve anything
  possible at that stage is run, in where existent large expressions get some
  of their subexpressions reduced to values, for the permanently solved ones.
  And so, the pre-processing is done.
- At this point, all values generated are supposed constants, so are stored
  internally as that, so for multiple evaluations all of the previous steps
  aren't necessary. Here is when the loading/downloading process ends,
  and object is instantiated and ready to use.

(3) Solve/evaluate logic

Set any input to different values is done by:
`logic[input_ref] = new_value`
where `input_ref` could be a variable name, or a cell reference like "E8".

When `logic.solve()` is called, the steps towards a new logic evaluation are:
- Reset of any intermediate value from previous evaluations
- Fill the values from cells considered **'inputs'** to the logic
(and make the eq system resoluble :)
- Loop over main equations, solving them recursively until it's done.

(4) Comparison, export & show info methods

As the old values are stored in a column of the main sheet, comparison between
evaluations is easily done, and old values can be replaced with new ones to
compare again against new changes. A `.variables_as_dict()` method is present
to generate a new dict `{"var_name": var_value}`.

### Known limitations

* Few implemented _Excel_ functions
* Cell formulae are seen as unique-cell, placement independent, equations,
so matrix functions don't play well (use with care).
* Sheet names are handled internally as a slugified form
(_`"_Something very-strange_"`_ is parsed as _`"Something_very_strange"`),
so it could raise conflicts if worksheets are named badly.

## Install

Clone the repo and `pip install -e .` it. If **inside a conda environment**,
first install pandas with `conda install pandas`, as it's one of the main
requirements, so setup it's quicker.

After that you'll need some environment variables to define your
Google Sheet credentials_ to access the spreadsheets you want to import.
Copy the example env file in the library root folder: `cp .env.example .env`,
and edit it:

```dotenv
# Credentials to access Google Sheet documents
GSHEET_PRIVATE_KEY_ID="xxxxxxxxxxxa"
GSHEET_PRIVATE_KEY="-----BEGIN PRIVATE KEY-----\n....\n-----END PRIVATE KEY-----\n"
GSHEET_CLIENT_ID=123456789012345678901
GSHEET_CLIENT_EMAIL="user.email@domain.com"

# Sheet key to use as default
GSHEET_DEFAULT_SHEET_ID="1liKwLA3Y5b5bvwaNkQyCpBHP6G2jl9ooAvaJa7dKxtI"
```

## Usage

Import the main object and instantiate a new `GSheetLogic` object with a
valid sheet ID to access the GSheet API and download and import the logic contained inside.

```python
from gs_eq_parser import GSheetLogic

# Download in creation
logic = GSheetLogic(
    sheet_name="_PETR_DATA",
    column_name="VARIABLE_NAME",
    column_value="VALUE",
    column_category="CATEGORY",
    sheet_key="xxxxxxxxxxxxxxxxxxxxxxxxx",
)

# Check equations
logic.solve()
logic.report()

# Save to disk
logic.save("example_sheet_representation_nb.json")
solved_variables = logic.variables_as_dict()
```

After that, recreation from disk is much easy and quicker:

```python
logic = GSheetLogic.load("example_sheet_representation_nb.json")
logic.solve()
logic.report()
solved_variables = logic.variables_as_dict()
```

To change input values for re-evaluation, use the item setter on the object to
set the new values over the main sheet coordinates or the unique variable name.

```python
from dictdiffer import diff

logic["_PETR_DATA!E10"] = "ER25C+2.2kW"

logic.solve()
logic.report()

new_solved_variables = logic.variables_as_dict()

# Show differences in all variables
print(list(diff(solved_variables, new_solved_variables)))
```

```jupyterpython
[('change', 'fan_GR_spider', (1, 0)),
 ('change', 'fan_motor_typ', (2, 1)),
 ('change', 'fan_id', (114866, '2.2kW')),
 ('change', 'fan_Dsa', (320, 257)),
 ('change', 'fan_DN', (360, 290)),
 ('change', 'fan_B2', (95, 76)),
 ('change', 'fan_LxL', (450, 400)),
 ('change', 'fan_L2', (411, 288)),
 ('change', 'fan_H', (450, 415)),
 ('change', 'fan_H1', (225, 215)),
 ('change', 'fan_T', (340, 460)),
 ('change', 'fan_T1', (340, 499)),
 ('change', 'fan_T3', (213, 338)),
 ('change', 'fan_T4', (15, 97)),
 ('change', 'fan_T7', (213, 173)),
 ('change', 'fan_D1', (253, 179)),
 ('change', 'fan_E1', (317, 257)),
 ('change', 'fan_difusor_vel', (31, 22)),
 ('change', 'fan_difusor_E1', (317, 233)),
 ('change', 'fan_ER', (0, 1)),
 ('change', 'fan_spider_A', (315, 250)),
 ('change', 'fan_spider_D', (252, 0)),
 ('change', 'fan_slbl_vyska', (1, 30)),
 ('change', 'fan_slbl_shore', (1, 30)),
 ('change', 'fan_hmotnost', (13, 31))]
```

## Tests and development

To run tests and code coverage analysis on the library, `pip install -e .[dev]`
it to install the extra requirements.

Run local tests with coverage with:

```bash
pytest --cov=gs_eq_parser --cov-report term --cov-report html
```

### Implement new formula functions

Only a small set of 'excel-like' functions are implemented, so it's very
probable to find any new unimplemented function when using this library.

Known functions are defined in `gs_eq_parser/cellfunctions.py`, as simple
_lambda_ functions with a number of parameters and optional checks of output
type and minimum and maximum number of parameters, so it's pretty easy to
define new ones, if they follow the standard syntax of
`FUNCTIONNAME(param1,param2,...,paramN)`.

Actually, the functions implemented are:

```python
KNOWN_FUNCTIONS = [
    "ABS",
    "AND",
    "ARRAY_CONSTRAIN",
    "CEILING",
    "CONCATENATE",
    "COS",
    "EXP",
    "FIND",
    "FLOOR",
    "HLOOKUP",
    "IF",
    "IFERROR",
    "INT",
    "LOG",
    "MATCH",
    "MAX",
    "MID",
    "MIN",
    "MROUND",
    "OR",
    "PI",
    "ROUND",
    "SIN",
    "SQRT",
    "TAN",
    "TEXT",
    "TODAY",
    "VALUE",
    "VLOOKUP"
]
```
